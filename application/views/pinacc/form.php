<div class="col-xl-8">
    <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
        <div class="mail-item d-flex pd-y-10 pd-x-20">
            <div class="pd-t-5"><img src="<?php echo base_url() . 'uploads/anggota_image/' . $data['lcphoto'] ?>" class="wd-60 " alt=""></div>
            <div class="mg-l-30">
                <h6 class="tx-14"><a href="" class="tx-inverse"><?php echo 'No. Anggota : '. $data['lcno_anggota'] ?></a></h6>
                <h6 class="tx-14"><a href="" class="tx-inverse"><?php echo 'Nama        : '. $data['lcnama'] ?></a></h6>
                <h6 class="tx-14"><a href="" class="tx-inverse"><?php echo 'Jabtan      : '. $data['lcjabatan'] ?></a></h6>
            </div>
        </div>
        <form name="kategoriform" enctype="multipart/form-data" id="selectForm" method="post" action="<?php echo $form_action; ?>">
            <div class="row">
                <label class="col-sm-4 form-control-label">Nama: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcnama" type="text" placeholder="Nama" disabled="true"
                           value='<?php echo set_value('lcnama', isset($data['lcnama']) ? $data['lcnama'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Jabatan: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcjabatan" type="text" placeholder="Jabatan" disabled="true"
                           value='<?php echo set_value('lcjabatan', isset($data['lcjabatan']) ? $data['lcjabatan'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Keterangan: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <textarea rows="3" class="form-control" name="lcket"  placeholder="Keterangan" disabled="true" ><?php echo set_value('lcket', isset($data['lcket']) ? $data['lcket'] : ''); ?></textarea>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Nilai Pinjaman: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcnilai_pinjam" type="text" placeholder="Nilai Pinjaman"
                           value='<?php echo set_value('lcnilai_pinjam', isset($data['lcnilai_pinjam']) ? $data['lcnilai_pinjam'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Status : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <?php echo form_dropdown('lcliststatus', $listjab, isset($data['lcliststatus']) ? $data['lcliststatus'] : '', 'class="form-control" '); ?>
                </div>
            </div><!-- row -->

            <input type="hidden" name="lcid"  value="<?php echo set_value('lcid', isset($data['lcid']) ? $data['lcid'] : ''); ?>" />
            <div class="form-layout-footer mg-t-30">
                <button type="submit" class="btn btn-info mg-r-5">Save</button>
                <button class="btn btn-secondary">Cancel</button>
            </div><!-- form-layout-footer -->
        </form>
    </div><!-- card -->
</div><!-- col-6 -->



