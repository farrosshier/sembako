<div class="col-xl-8">
    <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
        <h6 class="card-body-title"><?php echo $title ?></h6>
        <p class="mg-b-20 mg-sm-b-30"><?php echo $titleket ?></p>
        <form name="kategoriform" enctype="multipart/form-data" id="selectForm" method="post" action="<?php echo $form_action; ?>">
             <div class="row">
                <label class="col-sm-4 form-control-label">No. Anggota: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcno_anggota" type="text" placeholder="Nomor Anggota"
                           value='<?php echo set_value('lcno_anggota', isset($data['lcno_anggota']) ? $data['lcno_anggota'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Nama: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcnama" type="text" placeholder="Nama"
                           value='<?php echo set_value('lcnama', isset($data['lcnama']) ? $data['lcnama'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Alamat: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcalamat" type="text" placeholder="Alamat"
                           value='<?php echo set_value('lcalamat', isset($data['lcalamat']) ? $data['lcalamat'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">No HP: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcno_hp" type="text" placeholder="No HP"
                           value='<?php echo set_value('lcno_hp', isset($data['lcno_hp']) ? $data['lcno_hp'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Tempat Lahir: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lctemp_lahir" type="text" placeholder="Tempat Lahir"
                           value='<?php echo set_value('lctemp_lahir', isset($data['lctemp_lahir']) ? $data['lctemp_lahir'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Tgl Lahir: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control fc-datepicker" name="lctgl_lahir" type="text" placeholder="Tanggal Lahir"
                           value='<?php echo set_value('lctgl_lahir', isset($data['lctgl_lahir']) ? $data['lctgl_lahir'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Jenis Kelamain : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcjekel" type="text" placeholder="Jenis Kelamin"
                           value='<?php echo set_value('lcjekel', isset($data['lcjekel']) ? $data['lcjekel'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Jabatan : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <?php echo form_dropdown('lckode_jabatan', $listjab, isset($data['lckode_jabatan']) ? $data['lckode_jabatan'] : '', 'class="form-control" '); ?>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Keterangan: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <textarea rows="3" class="form-control" name="lcketerangan"  placeholder="Keterangan"><?php echo set_value('lcketerangan', isset($data['lcketerangan']) ? $data['lcketerangan'] : ''); ?></textarea>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">user login: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcuserlogin" type="email" placeholder="Email Address"
                           value='<?php echo set_value('lcuserlogin', isset($data['lcuserlogin']) ? $data['lcuserlogin'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">user password: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcuserpass" type="text" placeholder="User Password"
                           value='<?php echo set_value('lcuserpass', isset($data['lcuserpass']) ? $data['lcuserpass'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row mg-t-20">
                <label class="col-sm-4 form-control-label">Photo: <span class="tx-danger">*</span></label>
                <div class="col-lg-3 mg-t-40 mg-lg-t-0">
                    <label class="custom-file">
                        <input type="file"  name="img" accept="image">
                    </label>
                </div>
            </div>

            <div class="row mg-t-20">
                <?php
                if (isset($data['lcphoto'])) {
                    ?>
                    <img src="<?php echo base_url(); ?>uploads/anggota_image/<?php echo $data['lcphoto']; ?>" width="20%" id='blah' />  
                    <?php
                } else {
                    ?>
                    <img src="<?php echo base_url(); ?>uploads/anggota_image/default.jpg" width="20%" alt="" id='blah' />
                    <?php
                }
                ?> 
            </div>
            
            <input type="hidden" name="lcid"  value="<?php echo set_value('lcid', isset($data['lcid']) ? $data['lcid'] : ''); ?>" />
            <div class="form-layout-footer mg-t-30">
                <button type="submit" class="btn btn-info mg-r-5">Save</button>
                <button class="btn btn-secondary">Cancel</button>
            </div><!-- form-layout-footer -->
        </form>
    </div><!-- card -->
</div><!-- col-6 -->



