<div class="col-xl-8">
    <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
        <?php $datakurirx = $this->session->userdata('datakurir'); ?>
        <h6 class="card-body-title">No Anggota : <?php echo $datakurirx['lcnoanggota'] ?></h6>
        <div class="mail-item d-flex pd-y-10 pd-x-20">
            <div class="pd-t-5"><img src="<?php echo base_url() . 'uploads/anggota_image/' . $datakurirx['lcphotoanggota'] ?>" class="wd-48 rounded-circle" alt=""></div>
            <div class="mg-l-30">
                <h6 class="tx-14"><a href="" class="tx-inverse"><?php echo $datakurirx['lcnama'] ?></a></h6>
                <p class="tx-13 mg-b-10"><?php echo $datakurirx['lcalamat'] ?></p>
            </div>
        </div>

        <form name="kategoriform" id="selectForm" method="post" action="<?php echo $form_action; ?>">
            <div class="row">
                <label class="col-sm-4 form-control-label">Jenis Simpanan : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <?php echo form_dropdown('lcjenis_simpan', $listjenis, isset($data['lcjenis_simpan']) ? $data['lcjenis_simpan'] : '', 'class="form-control" onchange="viewjenis(this.value)"'); ?>
                </div>
            </div><!-- row -->
                <div id="viewjenis">
                 <?php
                         if ( isset($kurirdata) ){
                             echo $kurirdata ;
                         }
                        ?>
                </div>

            <input type="hidden" name="lcid"  value="<?php echo $datakurirx['lcid'] ?>" />
            <div class="form-layout-footer mg-t-30">
                <button type="submit" name="submit" class="btn btn-info mg-r-5">Simpan</button>
                <!--<a href="savearray" class="btn btn-info mg-r-5"><div>Simpan</div></a>-->
                <h3 id="success"></h3>
            </div><!-- form-layout-footer -->
        </form>
    </div><!-- card -->
</div><!-- col-6 -->



