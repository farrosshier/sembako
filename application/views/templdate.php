<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
   <?php $this->load->view('headdate'); ?>
</head>

<body class="withvernav">
<?php if(! $this->session->userdata('validated'))
                    redirect('login');
   ?>

     <?php  function do_logout(){
             $this->session->sess_destroy();
                 redirect('login');
             }
?>
<div class="bodywrapper">
    <?php $this->load->view('navigation_bar'); ?>
    
    <?php  $this->load->view('sidebar_menu'); ?>
    
    <?php $this->load->view($main_view); ?>

</div>    
</body>
<!-- END BODY -->
</html>