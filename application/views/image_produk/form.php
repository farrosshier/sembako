<div class="col-xl-8">
    <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
        <h6 class="card-body-title"><?php echo $title ?></h6>
        <p class="mg-b-20 mg-sm-b-30"><?php echo $titleket ?></p>
        <form name="kategoriform" enctype="multipart/form-data" id="selectForm" method="post" action="<?php echo $form_action; ?>">
            <div class="row">
                <label class="col-sm-4 form-control-label">Nama : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcnama" type="text" placeholder="Nama"
                           value='<?php echo set_value('lcnama', isset($data['lcnama']) ? $data['lcnama'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Urut : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcurut" type="text" placeholder="No Urut"
                           value='<?php echo set_value('lcurut', isset($data['lcurut']) ? $data['lcurut'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row mg-t-20">
                <label class="col-sm-4 form-control-label">Photo: <span class="tx-danger">*</span></label>
                <div class="col-lg-3 mg-t-40 mg-lg-t-0">
                    <label class="custom-file">
                        <input type="file"  name="img" accept="image">
                    </label>
                </div>
            </div>

            <div class="row mg-t-20">
                <?php
                if (isset($data['lcphoto'])) {
                    ?>
                    <img src="<?php echo base_url(); ?>uploads/listproduk_image/<?php echo $data['lcphoto']; ?>" width="20%" id='blah' />  
                    <?php
                } else {
                    ?>
                    <img src="<?php echo base_url(); ?>uploads/listproduk_image/default.jpg" width="20%" alt="" id='blah' />
                    <?php
                }
                ?> 
            </div>
            
            <input type="hidden" name="lcid"  value="<?php echo set_value('lcid', isset($data['lcid']) ? $data['lcid'] : ''); ?>" />
            <div class="form-layout-footer mg-t-30">
                <button type="submit" class="btn btn-info mg-r-5">Save</button>
                <button class="btn btn-secondary">Cancel</button>
            </div><!-- form-layout-footer -->
        </form>
    </div><!-- card -->
</div><!-- col-6 -->



