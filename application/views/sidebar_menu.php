<div class="am-sideleft">
            <ul class="nav am-sideleft-tab">
                <li class="nav-item">
                    <a href="#mainMenu" class="nav-link active"><i class="icon ion-ios-home-outline tx-24"></i></a>
                </li>
                <li class="nav-item">
                    <a href="#emailMenu" class="nav-link"><i class="icon ion-ios-email-outline tx-24"></i></a>
                </li>
                <li class="nav-item">
                    <a href="#chatMenu" class="nav-link"><i class="icon ion-ios-chatboxes-outline tx-24"></i></a>
                </li>
                <li class="nav-item">
                    <a href="#settingMenu" class="nav-link"><i class="icon ion-ios-gear-outline tx-24"></i></a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="mainMenu" class="tab-pane active">
                    <ul class="nav am-sideleft-menu">
                        <li class="nav-item">
                            <a href="<?php echo base_url() .'index.php/welcome' ?>" class="nav-link">
                                <i class="icon ion-ios-home-outline"></i>
                                <span>Dashboard</span>
                            </a>
                        </li><!-- nav-item -->
                        <li class="nav-item">
                            <a href="" class="nav-link with-sub">
                                <i class="icon ion-ios-gear-outline"></i>
                                <span>Master</span>
                            </a>
                            <ul class="nav-sub">
                                <li class="nav-item"><?php echo anchor('katagori','Kategori','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('brand','Brand','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('mitra','Mitra','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('unit','Unit','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('slider','Image Slider','class="nav-link"'); ?></li>
                            </ul>
                        </li><!-- nav-item -->
                        <li class="nav-item">
                            <a href="" class="nav-link with-sub">
                                <i class="icon ion-ios-filing-outline"></i>
                                <span>Data</span>
                            </a>
                            <ul class="nav-sub">
                                <li class="nav-item"><?php echo anchor('produk','Produk','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('londri','Londry','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('pinacc','Acc Pinjam','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('pinjam','Bayar Pinjam','class="nav-link"'); ?></li>
                            </ul>
                        </li><!-- nav-item -->  
                        <li class="nav-item">
                            <a href="" class="nav-link with-sub">
                                <i class="icon ion-ios-analytics-outline"></i>
                                <span>Report</span>
                            </a>
                            <ul class="nav-sub">
                                <li class="nav-item"><?php echo anchor('#','Simpan ','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('#','Pinjam','class="nav-link"'); ?></li>
                            </ul>
                        </li><!-- nav-item -->
                        <li class="nav-item">
                            <a href="" class="nav-link with-sub">
                                <i class="icon ion-ios-chatboxes-outline"></i>
                                <span>Mitra</span>
                            </a>
                            <ul class="nav-sub">
                                <li class="nav-item"><?php echo anchor('#','Mitra','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('#','Produk','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('#','Report','class="nav-link"'); ?></li>
                            </ul>
                        </li><!-- nav-item -->  
                        <li class="nav-item">
                            <a href="" class="nav-link with-sub">
                                <i class="icon ion-ios-chatboxes-outline"></i>
                                <span>Pelanggan</span>
                            </a>
                            <ul class="nav-sub">
                                <li class="nav-item"><?php echo anchor('#','Pelanggan','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('invoiceorder','Transaksi','class="nav-link"'); ?></li>
                                <li class="nav-item"><?php echo anchor('#','Report','class="nav-link"'); ?></li>
                            </ul>
                        </li><!-- nav-item -->  
                        <li class="nav-item">
                            <a href="" class="nav-link with-sub">
                                <i class="icon ion-ios-navigate-outline"></i>
                                <span>User</span>
                            </a>
                            <ul class="nav-sub">
                                <li class="nav-item"><?php echo anchor('user','User','class="nav-link"'); ?></li>
                            </ul>
                        </li><!-- nav-item -->
                     </ul>
                </div><!-- #mainMenu -->
                <div id="emailMenu" class="tab-pane">
                   
                </div><!-- #emailMenu -->
                <div id="chatMenu" class="tab-pane">
                    
                </div><!-- #chatMenu -->
                <div id="settingMenu" class="tab-pane">
             
                </div><!-- #settingMenu -->
            </div><!-- tab-content -->
        </div><!-- am-sideleft -->