<div class="col-xl-8">
    <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
        <h6 class="card-body-title"><?php echo $data['lcnama'] ?></h6>
        <p class="mg-b-20 mg-sm-b-30"><?php echo $data['lcketerangan'] ?></p>
        <form name="kategoriform" enctype="multipart/form-data" id="selectForm" method="post" action="<?php echo $form_action; ?>">
            <div class="row">
                <label class="col-sm-4 form-control-label">Harga Beli : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcharga_beli" type="text" placeholder="Harga Beli"
                           value='<?php echo set_value('lcharga_beli', isset($data['lcharga_beli']) ? $data['lcharga_beli'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Harga Jual : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcharga_jual" type="text" placeholder="Harga Jual"
                           value='<?php echo set_value('lcharga_jual', isset($data['lcharga_jual']) ? $data['lcharga_jual'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Biaya Shipping : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcbiaya_shipping" type="text" placeholder="Biaya Shipping"
                           value='<?php echo set_value('lcbiaya_shipping', isset($data['lcbiaya_shipping']) ? $data['lcbiaya_shipping'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Stok : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcstok" type="text" placeholder="Stok"
                           value='<?php echo set_value('lcstok', isset($data['lcstok']) ? $data['lcstok'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Unit : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <?php echo form_dropdown('lcunit', $listunit, isset($data['lcunit']) ? $data['lcunit'] : '', 'class="form-control" '); ?>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Diskon : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lcdiskon" type="text" placeholder="Diskon"
                           value='<?php echo set_value('lcdiskon', isset($data['lcdiskon']) ? $data['lcdiskon'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Diskon Type : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <?php echo form_dropdown('lcdiskon_type', $listdiskon, isset($data['lcdiskon_type']) ? $data['lcdiskon_type'] : '', 'class="form-control" '); ?>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Tax / Pajak : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input class="form-control" id="nama" name="lctax" type="text" placeholder="Tag / Pajak"
                           value='<?php echo set_value('lctax', isset($data['lctax']) ? $data['lctax'] : ''); ?>'>
                </div>
            </div><!-- row -->
            <div class="row">
                <label class="col-sm-4 form-control-label">Tax Type : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <?php echo form_dropdown('lctax_type', $listtax, isset($data['lctax_type']) ? $data['lctax_type'] : '', 'class="form-control" '); ?>
                </div>
            </div><!-- row -->
            
            <input type="hidden" name="lcid"  value="<?php echo set_value('lcid', isset($data['lcid']) ? $data['lcid'] : ''); ?>" />
            <div class="form-layout-footer mg-t-30">
                <button type="submit" class="btn btn-info mg-r-5">Save</button>
                <button class="btn btn-secondary">Cancel</button>
            </div><!-- form-layout-footer -->
        </form>
    </div><!-- card -->
</div><!-- col-6 -->



