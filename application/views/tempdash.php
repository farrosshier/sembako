<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <?php $this->load->view('header'); ?>
    </head>

    <body>
        <?php
        if (!$this->session->userdata('validated'))
            redirect('login');
        ?>

        <?php

        function do_logout() {
            $this->session->sess_destroy();
            redirect('login');
        }
        ?>

        <?php $this->load->view('navigation_bar'); ?>

        <?php $this->load->view('sidebar_menu'); ?>

        <div class="am-pagetitle">
            <h5 class="am-title"><?php echo $titlemenu ?></h5>

        </div><!-- am-pagetitle -->

        <div class="am-mainpanel">
            <div class="am-pagebody">
    <div class="row row-sm">
        <div class="col-lg-4">
            <div class="card">
                <div id="rs1" class="wd-100p ht-200"></div>
                <div class="overlay-body pd-x-20 pd-t-20">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">Today's Earnings</h6>
                            <p class="tx-12">November 21, 2017</p>
                        </div>
                        <a href="" class="tx-gray-600 hover-info"><i class="icon ion-more tx-16 lh-0"></i></a>
                    </div><!-- d-flex -->
                    <h2 class="mg-b-5 tx-inverse tx-lato">$12,212</h2>
                    <p class="tx-12 mg-b-0">Earnings before taxes.</p>
                </div>
            </div><!-- card -->
        </div><!-- col-4 -->
        <div class="col-lg-4 mg-t-15 mg-sm-t-20 mg-lg-t-0">
            <div class="card">
                <div id="rs2" class="wd-100p ht-200"></div>
                <div class="overlay-body pd-x-20 pd-t-20">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">This Week's Earnings</h6>
                            <p class="tx-12">November 20 - 27, 2017</p>
                        </div>
                        <a href="" class="tx-gray-600 hover-info"><i class="icon ion-more tx-16 lh-0"></i></a>
                    </div><!-- d-flex -->
                    <h2 class="mg-b-5 tx-inverse tx-lato">$28,746</h2>
                    <p class="tx-12 mg-b-0">Earnings before taxes.</p>
                </div>
            </div><!-- card -->
        </div><!-- col-4 -->
        <div class="col-lg-4 mg-t-15 mg-sm-t-20 mg-lg-t-0">
            <div class="card">
                <div id="rs3" class="wd-100p ht-200"></div>
                <div class="overlay-body pd-x-20 pd-t-20">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">This Month's Earnings</h6>
                            <p class="tx-12">November 1 - 30, 2017</p>
                        </div>
                        <a href="" class="tx-gray-600 hover-info"><i class="icon ion-more tx-16 lh-0"></i></a>
                    </div><!-- d-flex -->
                    <h2 class="mg-b-5 tx-inverse tx-lato">$72,118</h2>
                    <p class="tx-12 mg-b-0">Earnings before taxes.</p>
                </div>
            </div><!-- card -->
        </div><!-- col-4 -->
    </div><!-- row -->

    <div class="row row-sm mg-t-15 mg-sm-t-20">
        <div class="col-md-6">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">Bar Chart</h6>
                <p class="mg-b-20 mg-sm-b-30">A bar chart or bar graph is a chart with rectangular bars with lengths proportional to the values that they represent.</p>
                <div id="f2" class="ht-200 ht-sm-300"></div>
            </div><!-- card -->
        </div><!-- col-6 -->
        <div class="col-md-6 mg-t-15 mg-sm-t-20 mg-md-t-0">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">Line Chart</h6>
                <p class="mg-b-20 mg-sm-b-30">The stacked charts are used when data sets have to be broken down into their constituents.</p>
                <div id="f1" class="ht-200 ht-sm-300"></div>
            </div><!-- card -->
        </div><!-- col-6 -->
    </div><!-- row -->
            <div class="am-footer">
                <span>Copyright &copy; 2018 All Rights Reserved. Koprasi by nproject</span>
                <!--<span>Created by: ThemePixels, Inc.</span>-->
            </div><!-- am-footer -->
        </div><!-- am-mainpanel -->
        <?php $this->load->view('footer'); ?>
        <script>
            $(function () {

                'use strict';

                var rs1 = new Rickshaw.Graph({
                    element: document.querySelector('#rs1'),
                    renderer: 'area',
                    max: 80,
                    stroke: true,
                    series: [{
                            data: [
                                {x: 0, y: 20},
                                {x: 1, y: 10},
                                {x: 2, y: 15},
                                {x: 3, y: 10},
                                {x: 4, y: 15},
                                {x: 5, y: 5},
                                {x: 6, y: 15},
                                {x: 7, y: 10},
                                {x: 8, y: 20},
                                {x: 9, y: 25},
                                {x: 10, y: 35}
                            ],
                            color: '#999999',
                            stroke: '#37474F'
                        }]
                });
                rs1.render();

                // Responsive Mode
                new ResizeSensor($('.am-mainpanel'), function () {
                    rs1.configure({
                        width: $('#rs1').width(),
                        height: $('#rs1').height()
                    });
                    rs1.render();
                });

                var rs2 = new Rickshaw.Graph({
                    element: document.querySelector('#rs2'),
                    renderer: 'area',
                    max: 80,
                    stroke: true,
                    series: [{
                            data: [
                                {x: 0, y: 10},
                                {x: 1, y: 15},
                                {x: 2, y: 18},
                                {x: 3, y: 15},
                                {x: 4, y: 20},
                                {x: 5, y: 10},
                                {x: 6, y: 15},
                                {x: 7, y: 10},
                                {x: 8, y: 20},
                                {x: 9, y: 25},
                                {x: 10, y: 30}
                            ],
                            color: '#00FF40',
                            stroke: '#0B3B0B'
                        }]
                });
                rs2.render();

                // Responsive Mode
                new ResizeSensor($('.am-mainpanel'), function () {
                    rs2.configure({
                        width: $('#rs2').width(),
                        height: $('#rs2').height()
                    });
                    rs2.render();
                });

                var rs3 = new Rickshaw.Graph({
                    element: document.querySelector('#rs3'),
                    renderer: 'area',
                    max: 80,
                    stroke: true,
                    series: [{
                            data: [
                                {x: 0, y: 20},
                                {x: 1, y: 10},
                                {x: 2, y: 15},
                                {x: 3, y: 10},
                                {x: 4, y: 15},
                                {x: 5, y: 5},
                                {x: 6, y: 15},
                                {x: 7, y: 10},
                                {x: 8, y: 20},
                                {x: 9, y: 25},
                                {x: 10, y: 20}
                            ],
                            color: '#B43104',
                            stroke: '#8A0808'
                        }]
                });
                rs3.render();

                // Responsive Mode
                new ResizeSensor($('.am-mainpanel'), function () {
                    rs3.configure({
                        width: $('#rs3').width(),
                        height: $('#rs3').height()
                    });
                    rs3.render();
                });


                $.plot("#f2", [{
                        data: [[0, 3], [2, 8], [4, 5], [6, 13], [8, 5], [10, 7], [12, 8], [14, 10]],
                        bars: {
                            show: true,
                            lineWidth: 0,
                            fillColor: '#2D3A50'
                        }
                    }, {
                        data: [[1, 5], [3, 7], [5, 10], [7, 7], [9, 9], [11, 5], [13, 4], [15, 6]],
                        bars: {
                            show: true,
                            lineWidth: 0,
                            fillColor: '#FB9337'
                        }
                    }], {
                    grid: {
                        borderWidth: 1,
                        borderColor: '#D9D9D9'
                    },
                    yaxis: {
                        tickColor: '#d9d9d9',
                        font: {
                            color: '#666',
                            size: 10
                        }
                    },
                    xaxis: {
                        tickColor: '#d9d9d9',
                        font: {
                            color: '#666',
                            size: 10
                        }
                    }
                });


                var newCust = [[0, 2], [1, 3], [2, 6], [3, 5], [4, 7], [5, 8], [6, 10]];
                var retCust = [[0, 1], [1, 2], [2, 5], [3, 3], [4, 5], [5, 6], [6, 9]];

                var plot = $.plot($('#f1'), [{
                        data: newCust,
                        label: 'New Customer',
                        color: '#2D3A50'
                    },
                    {
                        data: retCust,
                        label: 'Returning Customer',
                        color: '#FB9337'
                    }],
                        {
                            series: {
                                lines: {
                                    show: true,
                                    lineWidth: 1
                                },
                                shadowSize: 0
                            },
                            points: {
                                show: false,
                            },
                            legend: {
                                noColumns: 1,
                                position: 'nw'
                            },
                            grid: {
                                hoverable: true,
                                clickable: true,
                                borderColor: '#ddd',
                                borderWidth: 0,
                                labelMargin: 5,
                                backgroundColor: '#fff'
                            },
                            yaxis: {
                                min: 0,
                                max: 15,
                                color: '#eee',
                                font: {
                                    size: 10,
                                    color: '#999'
                                }
                            },
                            xaxis: {
                                color: '#eee',
                                font: {
                                    size: 10,
                                    color: '#999'
                                }
                            }
                        });

            });
        </script>
    </body>
    <!-- END BODY -->
</html>