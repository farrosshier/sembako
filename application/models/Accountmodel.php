<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Accountmodel
 *
 * @author user
 */
class Accountmodel extends CI_Model {

    function accountmodel() {
        parent::__construct();
    }

    var $dataUsers = 'anggota';
    var $dataorderan = 'orderbar';
    var $prductorder = 'listorder';
    var $invoiceorder = 'invoiceorder';

    public function setLastActive($accountId) {
        $time = time();
        $dataAccount = array('activedate' => $time);
        $this->db->where('id_anggota', $accountId);
        $this->db->update($this->dataUsers, $dataAccount);
    }

    public function isEmailExists($user_email) {
        $this->db->where('userlogin', $user_email);
        $this->db->limit(1);
        $stmt = $this->db->get($this->dataUsers);

        if ($stmt->num_rows() > 0) {
            return true;
        }

        return false;
    }

    public function getLastId($email) {
        $this->db->select('id_anggota');
        $this->db->where('userlogin', $email);
        $this->db->limit(1);
        return $this->db->get($this->dataUsers);
    }

    public function isLoginExists($username) {
        $this->db->where('userlogin', $username);
        $this->db->limit(1);
        $stmt = $this->db->get($this->dataUsers);

        if ($stmt->num_rows() > 0) {

            return true;
        }

        return false;
    }

    public function signup($username, $fullname, $password, $telp, $language = '') {

        $result = array("error" => true);

//        $helper = new helper($this->db);
//        if (!$this->helper->isCorrectLogin($fullname)) {
//
//            $result = array("error" => true,
//                "error_code" => ERROR_UNKNOWN,
//                "error_type" => $fullname,
//                "error_description" => "Incorrect login");
//
//            return $result;
//        }

        if ($this->isLoginExists($username)) {

            $result = array("error" => true,
                "error_code" => ERROR_LOGIN_TAKEN,
                "error_type" => 0,
                "error_description" => "Login already taken");

            return $result;
        }

        if (empty($fullname)) {

            $result = array("error" => true,
                "error_code" => ERROR_UNKNOWN,
                "error_type" => 3,
                "error_description" => "Empty user full name");

            return $result;
        }

        if (!$this->helper->isCorrectPassword($password)) {

            $result = array("error" => true,
                "error_code" => ERROR_UNKNOWN,
                "error_type" => 1,
                "error_description" => "Incorrect password");

            return $result;
        }

        if (!$this->helper->isCorrectEmail($username)) {

            $result = array("error" => true,
                "error_code" => ERROR_UNKNOWN,
                "error_type" => 2,
                "error_description" => "Wrong email");

            return $result;
        }

        if ($this->isEmailExists($username)) {

            $result = array("error" => true,
                "error_code" => ERROR_EMAIL_TAKEN,
                "error_type" => 2,
                "error_description" => "User with this email is already registered");

            return $result;
        }

        $salt = $this->helper->generateSalt(10);
        $enscript1 = "AnggORmaH!";
        $enscript2 = "!Machfudh...?";
//            $password = $this->input->post('lcuserpass');
        $pass = md5($enscript1 . md5($password) . $enscript2);
        $passw_hash = md5(md5($pass) . $salt);
//        $passw_hash = md5(md5($password) . $salt);
        $currentTime = time();

        $ip_addr = $this->helper->ip_addr();

        $accountState = ACCOUNT_STATE_ENABLED;

        $dataAccount = array('userlogin' => $username,
            'nama' => $fullname,
            'userpass' => $passw_hash,
            'telp' => $telp,
            'salt' => $salt,
            'insertdate' => $currentTime,
            'activedate' => $currentTime);

        $stmt = $this->db->insert($this->dataUsers, $dataAccount);


        if ($stmt) {

            $setId = $this->getLastId($username)->row()->id_anggota;

            $result = array("error" => false,
                'accountId' => $setId,
                'userlogin' => $username,
                'userpass' => $password,
                'error_code' => ERROR_SUCCESS,
                'error_description' => 'SignUp Success!');

            return $result;
        }

        return $result;
    }

    public function signin($username, $password) {
        $access_data = array('error' => true);

        $username = $this->helper->clearText($username);
        $password = $this->helper->clearText($password);

        $stmt = $this->getSaltId($username)->num_rows();

        if ($stmt > 0) {

            $row = $this->getSaltId($username)->row();
            $enscript1 = "AnggORmaH!";
            $enscript2 = "!Machfudh...?";
            $pass = md5($enscript1 . md5($password) . $enscript2);
            $passw_hash = md5(md5($pass) . $row->salt);

            $stmt2 = $this->getLoginPass($username, $passw_hash)->num_rows();

            if ($stmt2 > 0) {

                $row2 = $this->getLoginPass($username, $passw_hash)->row();

                $access_data = array("error" => false,
                    "error_code" => ERROR_SUCCESS,
                    "accountid" => $row2->id_anggota);
            }
        }

        return $access_data;
    }

    public function getSaltId($username) {
        $this->db->select('salt');
        $this->db->where('userlogin', $username);
        $this->db->limit(1);
        return $this->db->get($this->dataUsers);
    }

    public function getLoginPass($username, $password) {
        $this->db->select('id_anggota');
        $this->db->where('userlogin', $username);
        $this->db->where('userpass', $password);
        $this->db->limit(1);
        return $this->db->get($this->dataUsers);
    }

    public function getanggota($accountId) {
        $result = array("error" => true,
            "error_code" => ERROR_ACCOUNT_ID);

        $row = $this->getDataId($accountId)->row();
        $stmt_row = $this->getDataId($accountId)->num_rows();

        if ($stmt_row > 0) {

            $notifications_count = 0;

//            $notifications_count = $this->notifymodel->getNewCount($accountId, $row->last_notify_view);


            $online = false;

            $current_time = time();

            if ($row->activedate != 0 && $row->activedate > ($current_time - 15 * 60)) {

                $online = true;
            }


            $result = array(
                "id_anggota" => $row->id_anggota,
                "nama" => stripcslashes($row->nama),
                "telp" => $row->telp,
                "userlogin" => $row->userlogin,
                "token" => $row->token,
                "new_token" => $row->new_token,
                "originPhotoUrl" => $row->originPhotoUrl,
                "normalPhotoUrl" => $row->normalPhotoUrl,
                "bigPhotoUrl" => $row->bigPhotoUrl,
                "lowPhotoUrl" => $row->lowPhotoUrl,
                "online" => $online);
        }
        return $result;
    }

    public function getalamat($accountId) {
        $result = array("error" => true,
            "error_code" => ERROR_ACCOUNT_ID);

        $this->db->where('id_anggota', $accountId);
        $lcalamat = $this->db->get('v_alamat');

        $row = $lcalamat->row();
        $stmt_row = $lcalamat->num_rows();

        if ($stmt_row > 0) {

            $notifications_count = 0;

//            $notifications_count = $this->notifymodel->getNewCount($accountId, $row->last_notify_view);


            $online = false;

            $current_time = time();

            $result = array(
                "id_anggota" => $row->id_anggota,
                "nama" => stripcslashes($row->nama),
                'jalan' => $row->jalan,
                'no_rumah' => $row->no_rumah,
                'namakota' => $row->namakota,
                'namakec' => $row->namakec,
                'namakel' => $row->namakel,
                'kodepos' => $row->kodepos,
                'longti' => $row->longti,
                'latitut' => $row->latitut);
        }

        return $result;
    }

    public function getDataId($accountId) {
        $this->db->where('id_anggota', $accountId);
        return $this->db->get($this->dataUsers);
    }

        
    public function insertinvoice($accountid,$data,$detail,$model) {
        $result = array("error" => false);

        $salt = $this->helper->generateInvoSalt(3);
        $currentTime = time();
        $invoiceno = $model.strtoupper($salt).$currentTime;
        $ip_addr = $this->helper->ip_addr();
        $invoiceorder = array(
            'id_anggota' =>$accountid,
            'nama' => $data['nama'],
            'phone' => $data['phone'],
            'catatan' => $data['comment'],
            'invoice_no' => $invoiceno,
            'invoice_date' => $currentTime,
            'id_pengiriman' => 0,
            'id_shipping' => 0,
            'shipping_date' => $data['date_ship'],
            'shipping_status' => 0,
            'shipping_ket' => $data['shipping'],
            'shipping_add' => $data['alamat'],
            'id_bank' => 0,
            'id_pengiriman' => 0,
            'vat' => 0,
            'vat_persen' => '',
            'payment_type' => 0,
            'payment_status' => 0,
            'payment_detail' => '',
            'grand_total' => $data['total_fees'],
            'status' => '1'
        );
        
        $stmt = $this->db->insert($this->invoiceorder, $invoiceorder);

        if($stmt){
            
            for ($i = 0; $i < count($detail); $i++) {
            $listorder = array(
                'invoice_no' => $invoiceno,
                'id_produk' => $detail[$i]['productid'],
                'nama' => $detail[$i]['nama'],
                'harga' => $detail[$i]['harga'],
                'qty' => $detail[$i]['qty']
            );
            $stmt = $this->db->insert($this->prductorder, $listorder);
            
            if ($stmt){
                $jumkar = strlen($data['total_fees']) - 3;
                $nilaitot = substr($data['total_fees'], 0, $jumkar ) . substr($invoiceno, -3, 6);
    
//                kirimemail($from,$from_name, $email, $sale_code, $text)
                $this->kirimemail("machfudh@gmail.com","machfudh@gmail.com", "machfudh@gmail.com", $invoiceno, "TESTING BAE");
//$this->kirimemail("machfudh@gmail.com","machfudh@gmail.com", "siti.latifah.taslim@gmail.com", $invoiceno, "TESTING BAE");
                
                $result = array(
               "error" => false,
               "accountid"=>$accountid,
               "invoice_no" => $invoiceno,
                "totalbayar" => $nilaitot
               );
            }
            }
        }
        
        

        return $result;
    }

    public function getHomeSlider() {
        return $this->db->get('v_homeslider');
    }

    public function getKatLevel($idcat) {
            $this->db->where('id_katagori', $idcat);
            $data = $this->db->get('v_katagori')->row();
            
            return substr($data->kode_katagori,0,$data->lavel);
    }
    
    public function getKatagori($parrent, $model) {
        if ($parrent == 0) {
            return $this->db->get('v_katagori1');
        } else {
            $this->db->where('parent', $parrent);
            return $this->db->get('v_katagori');
        }
    }

    public function getProduk($page, $count, $query, $catagory_id) {
        $kodeKat = $this->getKatLevel($catagory_id);
        if ($query != '') {
            $this->db->like('tag',$query);
        }
        $this->db->like('kode_katagori',$kodeKat, 'after');
        $this->db->limit($count,$page);
        return $this->db->get('v_produk');
    }
    
    public function getProduk1($page, $count, $query, $catagory_id) {
        $kodeKat = $this->getKatLevel($catagory_id);
        if ($query != '') {
            $this->db->like('tag',$query, 'after');
        }
        $this->db->like('kode_katagori',$kodeKat, 'after');
        $this->db->limit($count,$page);
        return $this->db->get('v_produk');
    }
    
    public function getProduk2($page, $count, $query, $catagory_id) {
        $kodeKat = $this->getKatLevel($catagory_id);
        if ($query != '') {
            $this->db->like('tag',$query, 'before');
        }
        $this->db->like('kode_katagori',$kodeKat, 'after');
        $this->db->limit($count,$page);
        return $this->db->get('v_produk');
    }

    public function getProdukId($idproduct) {
        $this->db->where('id_produk', $idproduct);
        return $this->db->get('v_produk');
    }

    public function getProdukView($idproduct) {
        $row = $this->getProdukId($idproduct)->row();
        $stmt_row = $this->getProdukId($idproduct)->num_rows();

        if ($stmt_row > 0) {

            $result = array(
                'id_produk' => $row->id_produk,
                'plu' => $row->plu,
                'nama' => $row->nama,
                'keterangan' => $row->keterangan,
                'deskripsi' => $row->deskripsi,
                'harga_jual' => $row->harga_jual,
                'biaya_shipping' => $row->biaya_shipping,
                'stok' => $row->stok,
                'unit' => $row->unit,
                'diskon' => $row->diskon,
                'diskon_type' => $row->diskon_type,
                'tax' => $row->tax,
                'tax_type' => $row->tax_type,
                'namakatagori' => $row->namakatagori,
                'namabrand' => $row->namabrand
            );

            $data = $this->getProductImage($row->id_produk);
            $datares = $data->result();

            $result['photos'] = array();
//                $arre = array();
            foreach ($datares as $rowima) {
                array_push($result['photos'], array(
                    'id_image_produk' => $rowima->id_image_produk,
                    'nama' => $rowima->nama,
                    'photo' => $rowima->photo
                ));
            }


            $result['mitra'] = $this->getMitraView($row->id_mitra);
            $result['brand'] = $this->getBrandView($row->id_brand);
        }


        return $result;
    }

    public function getMitraId($idmitra) {
        $this->db->where('id_mitra', $idmitra);
        return $this->db->get('v_mitra');
    }

    public function getMitraView($idmitra) {
        $row = $this->getMitraId($idmitra)->row();
        $stmt_row = $this->getMitraId($idmitra)->num_rows();
        if ($stmt_row > 0) {

            $result = array(
                'id_mitra' => $row->id_mitra,
                'nama' => $row->nama,
                'keterangan' => $row->keterangan,
                'area1' => $row->area1,
                'area2' => $row->area2,
                'area3' => $row->area3,
                'alamat' => $row->alamat,
                'photo' => $row->photo);
        }

        return $result;
    }

    public function getBrandId($idbrand) {
        $this->db->where('id_brand', $idbrand);
        return $this->db->get('v_brand');
    }

    public function getBrandView($idbrand) {
        $row = $this->getBrandId($idbrand)->row();
        $stmt_row = $this->getBrandId($idbrand)->num_rows();
        if ($stmt_row > 0) {

            $result = array(
                'id_brand' => $row->id_brand,
                'nama' => $row->nama,
                'keterangan' => $row->keterangan,
                'photo' => $row->photo);
        }

        return $result;
    }

    public function getProductImage($idproduct) {
        $this->db->where('id_produk', $idproduct);
//        $this->db->order_by('urut',desc);
        return $this->db->get('v_image_produk');
    }

    public function getCountProduk($query, $catagory_id) {
        if ($query != '') {
            
        }
        $kodeKat = $this->getKatLevel($catagory_id);
        $this->db->like('kode_katagori',$kodeKat, 'after');
        return $this->db->get('v_produk');
    }

    public function alamat($accountid, $nama, $jalan, $norumah, $kota, $kecamatan, $kelurahan, $longti, $latitut) {

        $result = array("error" => true, "error_code" => ERROR_ACCOUNT_ID);

        $dataalamat = array('id_anggota' => $accountid,
            'nama' => $nama,
            'jalan' => $jalan,
            'no_rumah' => $norumah,
            'kota' => $kota,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,
            'longti' => $longti,
            'latitut' => $latitut
        );

        $this->db->where('id_anggota', $accountid);
        $lcalamat = $this->db->get('alamat');

        if ($lcalamat->num_rows() > 0) {
            $this->db->where('id_anggota', $accountid);
            $stmt = $this->db->update('alamat', $dataalamat);
        } else {

            $stmt = $this->db->insert('alamat', $dataalamat);
        }

        if ($stmt) {
            $result = array("error" => false,
                'accountid' => $accountid,
                'error_code' => ERROR_SUCCESS,
                'error_description' => 'Insert Alamat Success!');
        }

        return $result;
    }

    public function getOrderSembako($page, $count, $account_id) {
        $this->db->where('id_anggota',$account_id);
        $this->db->limit($count,$page);
        return $this->db->get('v_invoiceorder');
    }
    
    public function getCountOrderSembako($account_id) {
        $this->db->where('id_anggota',$account_id);
        return $this->db->get('v_invoiceorder');
    }
    
    public function getAlamatList($page, $count, $account_id) {
        $this->db->where('id_anggota',$account_id);
        $this->db->limit($count,$page);
        return $this->db->get('v_alamat');
    }
    
    public function getCountAlamatList($account_id) {
        $this->db->where('id_anggota',$account_id);
        return $this->db->get('v_alamat');
    }
    
    
    public function logout($accountId, $accessToken) {
        $accountId = $this->helper->clearInt($accountId);

        $accessToken = $this->helper->clearText($accessToken);
//        $accessToken = helper::escapeText($accessToken);
        $currentTime = time();

        $u_agent = $this->helper->u_agent();
        $ip_addr = $this->helper->ip_addr();

        $dataToken = array('id_anggota' => $accountId,
            'token' => $accessToken,
            'waktu' => $currentTime,
            'kegiatan' => 'Logout',
            'catatan' => 'Logoout dr Android',
            'u_agent' => $u_agent,
            'ip_addr' => $ip_addr);

        $stmt = $this->db->insert('anggotalog', $dataToken);



//        $this->db->where('id_anggota', $accountId);
//        $this->db->where('token', $accessToken);
//        $this->db->update('kurirlog',$dataToken);
    }

    public function setPhoto($accountId, $array_data) {
        $dataPhoto = array('originPhotoUrl' => $array_data['originPhotoUrl'],
            'normalPhotoUrl' => $array_data['normalPhotoUrl'],
            'bigPhotoUrl' => $array_data['bigPhotoUrl'],
            'lowPhotoUrl' => $array_data['lowPhotoUrl']);

        $this->db->where('id_anggota', $accountId);
        $this->db->update($this->dataUsers, $dataPhoto);
    }
    
    public function kirimemail($from,$from_name, $email, $sale_code, $text) {
//        $email = $this->get_type_name_by_id('user', $this->get_type_name_by_id('sale', $sale_id, 'buyer'), 'email');
//        $sale_code = '#' . $this->get_type_name_by_id('sale', $sale_id, 'sale_code');
//        $from = $this->db->get_where('general_settings', array(
//                    'type' => 'system_email'
//                ))->row()->value;
//        $from_name = $this->db->get_where('general_settings', array(
//                    'type' => 'system_name'
//                ))->row()->value;
//        $page_data['sale_id'] = $sale_id;
//        $text = $this->load->view('front/shopping_cart/invoice_email', $page_data, TRUE);
//        $from,$from_name, $email, $sale_code, $text
        
        $this->email_model->do_email($from, $from_name, $email, $sale_code, $text);
//        $admins = $this->db->get_where('admin', array('role' => '1'))->result_array();
//        foreach ($admins as $row) {
//            $this->email_model->do_email($from, $from_name, $row['email'], $sale_code, $text);
//        }
    }

}
