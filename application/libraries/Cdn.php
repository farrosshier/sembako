<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cdn
 *
 * @author user
 */
class cdn {

    private $ftp_url = "";
    private $ftp_server = "";
    private $ftp_user_name = "";
    private $ftp_user_pass = "";
    private $cdn_server = "";
    private $conn_id = false;

//    public function __construct() {
//        $this->conn_id = @ftp_connect($this->ftp_server);
//
//        parent::__construct();
//    }

    public function upload($file, $remote_file) {
        $remote_file = $this->cdn_server . $remote_file;

        if ($this->conn_id) {

            if (@ftp_login($this->conn_id, $this->ftp_user_name, $this->ftp_user_pass)) {

                // upload a file
                if (@ftp_put($this->conn_id, $remote_file, $file, FTP_BINARY)) {

                    return true;
                } else {

                    return false;
                }
            }
        }
    }

    public function uploadMyPhoto($imgFilename) {
        rename($imgFilename, MY_PHOTOS_PATH . basename($imgFilename));

        $result = array("error" => false,
            "error_code" => ERROR_SUCCESS,
            "fileUrl" => APP_URL . "/" . MY_PHOTOS_PATH . basename($imgFilename));

        return $result;
    }

    public function uploadPhoto($imgFilename) {
        rename($imgFilename, PHOTO_PATH . basename($imgFilename));

//        $result = array("error" => false,
//            "error_code" => ERROR_SUCCESS,
//            "fileUrl" => APP_URL . "/" . PHOTO_PATH . basename($imgFilename));
        
         $result = array("error" => false,
            "error_code" => ERROR_SUCCESS,
            "fileUrl" => basename($imgFilename));

        return $result;
    }

    public function uploadCover($imgFilename) {
        rename($imgFilename, COVER_PATH . basename($imgFilename));

        $result = array("error" => false,
            "error_code" => ERROR_SUCCESS,
            "fileUrl" => APP_URL . "/" . COVER_PATH . basename($imgFilename));

        return $result;
    }

    public function uploadPostImg($imgFilename) {
        rename($imgFilename, POST_PHOTO_PATH . basename($imgFilename));

        $result = array("error" => false,
            "error_code" => ERROR_SUCCESS,
            "fileUrl" => APP_URL . "/" . POST_PHOTO_PATH . basename($imgFilename));

        return $result;
    }

    public function uploadChatImg($imgFilename) {
        rename($imgFilename, CHAT_IMAGE_PATH . basename($imgFilename));

        $result = array("error" => false,
            "error_code" => ERROR_SUCCESS,
            "fileUrl" => APP_URL . "/" . CHAT_IMAGE_PATH . basename($imgFilename));

        return $result;
    }

}

?>
