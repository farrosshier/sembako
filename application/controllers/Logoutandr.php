<?php
require APPPATH . '/libraries/REST_Controller.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Logoutandr
 *
 * @author user
 */
class Logoutandr extends REST_Controller {
     function __construct($config = 'rest') {
        parent::__construct($config);
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }
    
    function index_post() {
        $clientid = $this->post('clientid');
        if (empty($clientid))
            $clientid = 0;
        
        $accountid = $this->post('accountid');
        if (empty($accountid ))
            $accountid  = '';

        $token = $this->post('token');
        if (empty($token))
            $token = '';


        $clientid = $this->helper->clearInt($clientid);
        $accountid = $this->helper->clearInt($accountid);
        $token = $this->helper->clearText($token);

        if ($clientid != CLIENT_ID) {

            $this->helpel->printError(ERROR_UNKNOWN, CLIENT_ID + " Error client Id.");
        }
        
        $access_data = $this->authmodel->authorize($accountid, $token);
        
         if ( $access_data['error'] === false) {
            $this->helpel->printError(ERROR_ACCESS_TOKEN, "Error authorization.");
        }

//        echo " lewatin error nich";
        $this->accountmodel->logout($accountid,$token);
        
        $result = array("error" => false,
                    "error_code" => ERROR_SUCCESS);


    echo json_encode($result);
    exit;
        
        
    }
}
