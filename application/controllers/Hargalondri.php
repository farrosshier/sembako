<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hargalondri
 *
 * @author user
 */
class Hargalondri extends CI_Controller{
    function __construct() {
        parent::__construct();
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }

    var $limit = 10;
    var $title = 'Londry';
    var $titleket = 'Londry';
    var $linkweb = 'Londry';
    var $titlemenu = 'Master Data';
    var $tabel = 'londri';

    function index() {
        $this->listdata();
    }

    function listdata($offset = 0) {
        
    }
    
    function action($para1 = '', $para2 = '') {
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titleket'] = $this->titleket;

        if ($para1 == 'update') {
            
            $timeinsert = time();
            $datatabel = array(
                'harga_beli' => $this->input->post('lcharga_beli'),
                'harga_jual' => $this->input->post('lcharga_jual'),
                'harga_kilo_beli' => $this->input->post('lcharga_kilo_beli'),
                'harga_kilo_jual' => $this->input->post('lcharga_kilo_jual'),
                'biaya_shipping' => $this->input->post('lcbiaya_shipping'),
                'tempo' => $this->input->post('lctempo'),
                'diskon' => $this->input->post('lcdiskoni'),
                'diskon_type' => $this->input->post('lcdiskon_type'),
                'tax' => $this->input->post('lctax'),
                'tax_type' => $this->input->post('lctax_type'),
                'insertdate' => $timeinsert,
                'userinsert' => $this->session->userdata('iduserlog')
            );
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->update($this->tabel, $datatabel);
            redirect($this->tabel);
        } elseif ($para1 == 'edit') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = $this->tabel . '/formharga';
            $data['form_action'] = site_url('harga'.$this->tabel . '/action/update/' . $para2);
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->get($this->tabel);
            $lcDataList = $this->crudmodel->get_data_by_id($this->tabel, $para2)->row();
            $data['data']['lcid'] = $lcDataList->id_londri;
            $data['data']['lcnama'] = $lcDataList->nama;
            $data['data']['lcketerangan'] = $lcDataList->keterangan;
            $data['data']['lcharga_beli'] = $lcDataList->harga_beli;
            $data['data']['lcharga_jual'] = $lcDataList->harga_jual;
            $data['data']['lcharga_kilo_beli'] = $lcDataList->harga_kilo_beli;
            $data['data']['lcharga_kilo_jual'] = $lcDataList->harga_kilo_jual;
            $data['data']['lcbiaya_shipping'] = $lcDataList->biaya_shipping;
            $data['data']['lctempo'] = $lcDataList->tempo;
            $data['data']['lcdiskon'] = $lcDataList->diskon;
            $data['data']['lcdiskon_type'] = $lcDataList->diskon_type;
            $data['data']['lctax'] = $lcDataList->tax;
            $data['data']['lctax_type'] = $lcDataList->tax_type;
            
            $lsunit = $this->db->get('unit')->result();
            $num_row = $this->db->get('unit')->num_rows();
            if ($num_row > 0) {
                foreach ($lsunit as $row) {
                    $data['listunit'][$row->id_unit] = $row->nama;
                }
            }
            
            $data['listdiskon'][1] = "Nilai";
            $data['listdiskon'][3] = "Percent";
            
            $data['listtax'][1] = "Nilai";
            $data['listtax'][3] = "Percent";
            $this->load->view('tempfroms', $data);
        } 
    }
}
