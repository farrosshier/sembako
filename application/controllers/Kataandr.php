<?php
require APPPATH . '/libraries/REST_Controller.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Kataandr
 *
 * @author user
 */
class Kataandr  extends REST_Controller {
     function __construct($config = 'rest') {
        parent::__construct($config);
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }
    
    function index_post() {
        
        $clientid = $this->post('clientid');
        if (empty($clientid))
            $clientid = 0;

        $token = $this->post('token');
        if (empty($token))
            $token = 0;
        
        $parrent = $this->post('parrent');
        if (empty($parrent))
            $parrent = 0;
        
        
        $model = $this->post('model');
        if (empty($model))
            $model = 'SALE';
        
        $clientid = $this->helper->clearInt($clientid);
        $token = $this->helper->clearText($token);
        
        $parrent = $this->helper->clearInt($parrent);
        $parrent = $this->helper->clearText($parrent);
        
        
          if ($clientid != CLIENT_ID) {

            $this->helpel->printError(ERROR_UNKNOWN, CLIENT_ID + " Error client Id.");
        }

        $access_data = $this->authmodel->tokentoid($token);
        
        if ($access_data["error"] === false) {
            
            
//            $access_data = array("error" => true,"error_code" => ERROR_ACCOUNT_ID);

            $data = $this->accountmodel->getKatagori($parrent,$model);
            $datares = $data->result();
               
//                if ($access_data['error'] === false) {
                
                $this->accountmodel->setLastActive($access_data['accountid']);
                $access_data['data'] = array();
//                $arre = array();
                foreach($datares as $row ){
//                       $arre = array(
//                           'id_slider' => $row->id_slider,
//                           'nama' => $row->nama,
//                           'keterangan' => $row->keterangan,
//                           'photo_kecil' => $row->photo_kecil
//                       );
                    
                      array_push($access_data['data'], array(
                           'id_kata' => $row->id_katagori,
                           'nama' => $row->nama,
                           'keterangan' => $row->keterangan,
                           'kode_katagori' => $row->kode_katagori,
                           'photo' => $row->photo
                       )); 
                }

//                $result = array_push($access_data['data'], $this->accountmodel->getanggota($access_data['accountid']));

//                $result = array("status" => 200, "message" => TRUE, "anggota" => $this->accountmodel->getanggota($access_data['accountid']));
//                }
                
                }else{
            
             $access_data = array("error" => true,
                "error_code" => ERROR_UNKNOWN,
                "error_type" => 1,
                "error_description" => "Account Id error");
            
            
        }
        
        $this->authmodel->show_response($access_data);
        
        
    }
}
