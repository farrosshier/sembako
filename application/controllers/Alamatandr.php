<?php
require APPPATH . '/libraries/REST_Controller.php';
/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Alamatandr
 *
 * @author user
 */
class Alamatandr extends REST_Controller {
     function __construct($config = 'rest') {
        parent::__construct($config);
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }
    
    function index_post() {

        $clientid = $this->post('clientid');
        if (empty($clientid))
            $clientid = 0;

        $accountid = $this->post('accountid');
        if (empty($accountid))
            $accountid = 0;

        $token = $this->post('token');
        if (empty($token))
            $token = 0;
        
        $nama = $this->post('nama');
        if (empty($nama))
            $nama = '';
        
        $jalan = $this->post('jalan');
        if (empty($jalan))
            $jalan = '';

        $norumah = $this->post('norumah');
        if (empty($norumah))
            $norumah = '';

        $kota = $this->post('kota');
        if (empty($kota))
            $kota = '';
        
        $kecamatan = $this->post('kecamatan');
        if (empty($kecamatan))
            $kecamatan = '';
        
        $kelurahan = $this->post('kelurahan');
        if (empty($kelurahan))
            $kelurahan = '';
        
        $kode = $this->post('kode');
        if (empty($kode))
            $kode = '';
        
        $longti = $this->post('longti');
        if (empty($longti))
            $longti = '';
        
        $latitut = $this->post('latitut');
        if (empty($latitut))
            $latitut = '';

        $clientid = $this->helper->clearInt($clientid);
        $accountid = $this->helper->clearInt($accountid);

        $token = $this->helper->clearText($token);
        $nama = $this->helper->clearText($nama);
        $jalan = $this->helper->clearText($jalan);
        $norumah = $this->helper->clearText($norumah);
        $kota = $this->helper->clearText($kota);
        $kecamatan = $this->helper->clearText($kecamatan);
        $kelurahan = $this->helper->clearText($kelurahan);
        $longti = $this->helper->clearText($longti);
        $latitut = $this->helper->clearText($latitut);
        
        if ($clientid != CLIENT_ID) {

            $this->helpel->printError(ERROR_UNKNOWN, CLIENT_ID + " Error client Id.");
        }

        $access_data = $this->authmodel->tokentoid($token);
        
        if ($access_data["error"] === false) {
//

            if ($access_data['accountid'] == $accountid) {
                
                
                $access_data = $this->accountmodel->alamat($access_data['accountid'], $nama, $jalan, $norumah, $kota, $kecamatan, $kelurahan, $longti, $latitut);
               
                if ($access_data['error'] === false) {
                
                $this->accountmodel->setLastActive($access_data['accountid']);
                $access_data['data'] = $this->accountmodel->getalamat($access_data['accountid']);
                $arre = array();

//                $result = array_push($access_data['data'], $this->accountmodel->getanggota($access_data['accountid']));

//                $result = array("status" => 200, "message" => TRUE, "anggota" => $this->accountmodel->getanggota($access_data['accountid']));
                }
                
                }else{
            
             $access_data = array("error" => true,
                "error_code" => ERROR_UNKNOWN,
                "error_type" => 1,
                "error_description" => "Account Id error");
        }
            
        }

//        $result = array("status" => 200, "message" => "ancur ancuran ", "data" => $arre);
//        echo $result;
        $this->authmodel->show_response($access_data);
        
//        echo json_encode($access_data);
//        exit;
    }
}
