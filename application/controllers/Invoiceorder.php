<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Invoiceorder
 *
 * @author user
 */
class Invoiceorder extends CI_Controller {

    function __construct() {
        parent::__construct();
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }
    
    var $limit = 10;
    var $title = 'Invoice';
    var $titleket = 'Transaksi';
    var $linkweb = 'Invoiceorder';
    var $titlemenu = 'Transaksi';
    var $tabel = 'invoiceorder';

    function index() {
        $this->listdata();
    }

    function listdata($offset = 0) {
        ;
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titlemenu'] = $this->titlemenu;
        $data['main_view'] = 'tabel';
        $data['form_action'] = site_url($this->linkweb . '/searchdata');
        $data['search'] = array('kode_kurir' => 'Code',
            'nama' => 'Name',
            'alamat' => 'Address',
            'telp1' => 'Phone Number',
            'nomor_kend' => 'Nomor Kendaraan',
        );
        $data['sfocus'] = 'kode_kurir';
        $finds = $this->session->userdata('finds');
        $findt = $this->session->userdata('findt');

        if ($this->session->userdata('caridata') == 'cr' . $this->tabel) {
            $finds = $this->session->userdata('finds');
            $findt = $this->session->userdata('findt');
        } else {
            $finds = '';
            $findt = '';
        }
        $uri_segment = 3;
        $offset = $this->uri->segment($uri_segment);

        $mDatalist = $this->crudmodel->list_data($this->tabel, $this->limit, $offset, $finds, $findt)->result();
        $numRow = $this->crudmodel->count_data($this->tabel);
        if ($numRow > 0) {
            $config['base_url'] = site_url('invoiceorder/listdata');
            $config['total_rows'] = $numRow;
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;
            $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
            $config['first_tag_open'] = ' <li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = '<i class="fa fa-angle-right"></i>';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item">';
            $config['cur_tag_close'] = '</li>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination'] = ' Total Record ' . $numRow . "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" . $this->pagination->create_links();
            $tmpl = array('table_open' => '<table class="table table-hover table-bordered mg-b-0">',
                'heading_row_start' => '<thead class="bg-info"><tr>',
                'heading_row_end' => '</tr></thead>',
                'heading_cell_start' => '<th>',
                'heading_cell_end' => '</th>',
                'row_start' => '<tr>',
                'row_end' => '</tr>',
                'row_alt_start' => '<tr>',
                'row_alt_end' => '</tr>'
            );
            $this->table->set_template($tmpl);
            $this->table->set_heading(
                    array('data' => 'No', 'style' => 'width:3%'), 
                    array('data' => 'Invoice No.', 'style' => 'width:7%'),
                    array('data' => 'Tgl Invoice', 'style' => 'width:7%'),
                    array('data' => 'Nama Pembeli', 'style' => 'width:7%'), 
                    array('data' => 'No HP', 'style' => 'width:20%'), 
                    array('data' => 'Catatan', 'style' => 'width:40%'), 
                    array('data' => 'Total Harga', 'style' => 'width:7%'), 
                    array('data' => 'Status', 'style' => 'width:4%')); 
//                    array('data' => '', 'style' => 'width:7%'));    
            $i = 0 + $offset;
            foreach ($mDatalist as $sDataList) {
                if ($this->session->userdata('access') == '257') {
                $this->table->add_row(++$i, $sDataList->invoice_no , date("d-m-Y", $sDataList->invoice_date), $sDataList->nama, $sDataList->phone, $sDataList->catatan, $sDataList->grand_total,
                            anchor($this->tabel . '/action/edit/' . $sDataList->id_invoiceorder, $sDataList->status == 1 ? "Menuggu" : "Lunas") . "&nbsp&nbsp&nbsp" 
//                            anchor($this->tabel . '/action/delete/' . $sDataList->id_anggota, '<i class="icon ion-trash-a"></i>', array('class' => "delete-row", 'data-original-title' => 'Delete', 'onclick' => "return confirm('Anda yakin akan menghapus data ini?')"))
                    );
                } 
            }

            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data !';
        }
        $data['link'] = array('link_add' => anchor($this->tabel . '/action/add', '<div><i class="fa fa-plus"></i></div>', 'class="btn btn-outline-success btn-icon mg-r-5"'),
            'link_print' => anchor($this->tabel . '/action/add', 'Print', 'class="btn btn-success btn-small hidden-phone"'));
        $this->load->view('templates', $data);
    }
    
    
     function action($para1 = '', $para2 = '') {
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titleket'] = $this->titleket;

        if ($para1 == 'save') {
            $salt = $this->authmodel->generateSalt(10);
            $timeinsert = time();
            $datatabel = array(
                'nama' => $this->input->post('lcnama'),
                'ket' => $this->input->post('lcket'),
                'nilai_pinjam' => $this->input->post('lcnilai_pinjam'),
                'tgl_insert' => $timeinsert,
                'userinsert' => $this->session->userdata('iduserlog'),
            );
            $this->db->insert($this->tabel, $datatabel);
//            $this->crudmodel->set_category_data(0);
//            recache();
            redirect($this->tabel);
        } elseif ($para1 == 'update') {
            $salt = $this->authmodel->generateSalt(10);
            $timeinsert = time();
            $data = array(
                'payment_status' => $this->input->post('lcstatus'),
                'payment_type' => $this->input->post('lctype'),
                'payment_detail' => $this->input->post('lcdetail'),
                'payment_date' => $timeinsert,
                'id_bank' => $this->input->post('lcbank'),
                'vat' => $this->input->post('lcvat'),
                'status' => 2
            );
            $this->db->where('id_'.$this->tabel, $para2);
            $savedata = $this->db->update($this->tabel, $data);
            
            $this->db->where('id_'.$this->tabel, $para2);
            $this->db->get($this->tabel);
            $lcDataList = $this->crudmodel->get_data_by_id($this->tabel, $para2)->row();
            
            $segno = $lcDataList->invoice_no;
            $noref = $lcDataList->invoice_date;
            $nama = $lcDataList->nama;
            $alamat1 = $lcDataList->shipping_add;
            $type = "SEM";
            $serv_type = "ANTAR SEMBAKO";
            $harga = 4000;
            
            if($savedata){
               
                $url = 'http://192.168.1.9/kurir/index.php/Insertbarangadd'; 
                $fields_string = "";
                $fields = array(
                  'clientid' => '2101',
                  'sharedkey' => 'BANK@d0e5bec1734e5cd99003dfc36a107e09',  
                  'segno' => $segno,
                  'noref' => $noref,
                  'nama' => $nama,
                  'alamat1' => $alamat1,
                  'type' => $type,
                  'serv_type' => $serv_type,
                  'harga' => $harga,
                    
           );
		   
		    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                    rtrim($fields_string, '&');
				
				 $ch = curl_init();

                //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL, $url);
			//	curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch,CURLOPT_POST, count($fields));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
			//	curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
				
                //execute post
                $result = curl_exec($ch);
				
                //close connection
                curl_close($ch);

				echo " Di terima json <br />";
			    echo $result;
				echo " <br />";
				echo " <br />";
				echo " <br />";
				echo " <br />";
				$data = json_decode($result, true);
				if($data['status']){
				echo " Di buat array dan di tampilkan  : <br />";
			// 	echo $data['nama'];
				echo "seqno  : ". $data['seqno'] ." <br />";
				echo "nama  : ". $data['nama'] ." <br />";
				echo "alamat1  : ". $data['alamat1'] ." <br />";
				echo "waktu_ambil  : ". $data['waktu_ambil'] ." <br />";
				}
                
            }
            
//            $this->crudmodel->set_category_data(0);
//            recache();
            redirect($this->tabel);
        } elseif ($para1 == 'add') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = $this->tabel . '/form';
            $data['form_action'] = site_url($this->tabel . '/action/save/');
            $this->load->view('tempfroms', $data);
        } elseif ($para1 == 'edit') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = $this->tabel . '/form';
            $data['form_action'] = site_url($this->tabel . '/action/update/' . $para2);
            $this->db->where('id_'.$this->tabel, $para2);
            $this->db->get($this->tabel);
            $lcDataList = $this->crudmodel->get_data_by_id($this->tabel, $para2)->row();
            $data['data']['lcid'] = $lcDataList->id_invoiceorder;
            $data['data']['lcstatus'] = $lcDataList->payment_status;
            $data['data']['lcstype'] = $lcDataList->payment_type;
            $data['data']['lcdetail'] = $lcDataList->payment_detail;
            $data['data']['lcbank'] = $lcDataList->id_bank;
            $data['data']['lcvat'] = $lcDataList->vat;
            
            
            
            
            $data['liststatus'][1] = "Sudah Masuk";
            $data['liststatus'][2] = "Sudah Di Terima Mitra ";
            $data['liststatus'][3] = "Gantung";
            
            $data['listtype'][1] = "COD";
            $data['listtype'][2] = "Chas Transfer";
            $data['listtype'][3] = "Kartu Kredit";
            
            $data['listbank'][1] = "BCA Bank";
            $data['listbank'][2] = "BNI Bank";
            $data['listbank'][3] = "BRI Bank";
            
            
            
            $this->load->view('tempfroms', $data);
        } elseif ($para1 = 'delete') {
            $this->db->where('id_'.$this->tabel, $para2);
            $this->db->delete($this->tabel);
//            $this->crudmodel->set_category_data(0);
            redirect($this->tabel);
        }
    }
}
