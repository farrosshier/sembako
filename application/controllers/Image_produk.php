<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Image_produk
 *
 * @author user
 */
class Image_produk extends CI_Controller{
     function __construct() {
        parent::__construct();
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }

    var $limit = 10;
    var $title = 'Image Produk';
    var $titleket = 'Image Produk';
    var $linkweb = 'Image_produk';
    var $titlemenu = 'Master Data';
    var $tabel = 'image_produk';

    function index() {
        $this->listdata();
    }

    function listdata($offset = 0) {
        ;
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titlemenu'] = $this->titlemenu;
        $data['main_view'] = 'tabel';
        $data['form_action'] = site_url($this->linkweb . '/searchdata');
        $data['search'] = array('kode_kurir' => 'Code',
            'nama' => 'Name',
            'alamat' => 'Address',
            'telp1' => 'Phone Number',
            'nomor_kend' => 'Nomor Kendaraan',
        );
        $data['sfocus'] = 'kode_kurir';
        $finds = $this->session->userdata('finds');
        $findt = $this->session->userdata('findt');
        
        $lcid = $this->session->userdata('produkimage');
        $lclevel0 = $this->crudmodel->get_data_by_id('produk', $lcid)->row(); 
        
       $data['title'] = 'Sub '. $lclevel0->nama ;
//        
//            echo $lclevel0->lavel;
//        echo "</br>";
//        echo $lclavel;
        

        if ($this->session->userdata('caridata') == 'cr' . $this->tabel) {
            $finds = $this->session->userdata('finds');
            $findt = $this->session->userdata('findt');
        } else {
            $finds = '';
            $findt = '';
        }
        $uri_segment = 3;
        $offset = $this->uri->segment($uri_segment);

        $mDatalist = $this->crudmodel->list_imageproduk($this->tabel, $this->limit, $offset, $finds, $findt,$lcid)->result();
        $numRow = $this->crudmodel->count_imageproduk($this->tabel, $this->limit, $offset, $finds, $findt,$lcid)->num_rows();
        if ($numRow > 0) {
            $config['base_url'] = site_url('kurir/listdata');
            $config['total_rows'] = $numRow;
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;
            $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
            $config['first_tag_open'] = ' <li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = '<i class="fa fa-angle-right"></i>';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item">';
            $config['cur_tag_close'] = '</li>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination'] = ' Total Record ' . $numRow . "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" . $this->pagination->create_links();
            $tmpl = array('table_open' => '<table class="table table-hover table-bordered mg-b-0">',
                'heading_row_start' => '<thead class="bg-info"><tr>',
                'heading_row_end' => '</tr></thead>',
                'heading_cell_start' => '<th>',
                'heading_cell_end' => '</th>',
                'row_start' => '<tr>',
                'row_end' => '</tr>',
                'row_alt_start' => '<tr>',
                'row_alt_end' => '</tr>'
            );
            $this->table->set_template($tmpl);
            $this->table->set_heading(
                    array('data' => 'Index', 'style' => 'width:3%'), 
                    array('data' => 'Image', 'style' => 'width:7%'), 
                    array('data' => 'Nama'), 
                    array('data' => '', 'style' => 'width:10%'));
            $i = 0 + $offset;
            foreach ($mDatalist as $sDataList) {
                if ($this->session->userdata('access') == '257') {
                    $this->table->add_row($sDataList->urut, '<img src="' . base_url() . 'uploads/listproduk_image/' . $sDataList->photo . '" class="wd-40" alt="Image">', anchor($this->tabel . '/action/edit/' . $sDataList->id_image_produk, $sDataList->nama), 
                            anchor($this->tabel . '/action/edit/' . $sDataList->id_image_produk, '<i class="icon ion-edit"></i>', array('class' => "edit-row", 'data-original-title' => 'Edit')) . "&nbsp&nbsp&nbsp" .
                            anchor($this->tabel . '/action/delete/' . $sDataList->id_image_produk, '<i class="icon ion-trash-a"></i>', array('class' => "delete-row", 'data-original-title' => 'Delete', 'onclick' => "return confirm('Anda yakin akan menghapus data ini?')"))
                    );
                } else if ($this->session->userdata('access') == '100') {
                    $this->table->add_row(++$i, '<img src="' . base_url() . 'uploads/katagori_image/' . $sDataList->photo . '" class="wd-40" alt="Image">', $sDataList->nama,$sDataList->keterangan, $sDataList->halaman
                    );
                }
            }

            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data !';
        }
        $data['link'] = array('link_add' => anchor($this->tabel . '/action/add', '<div><i class="fa fa-plus"></i></div>', 'class="btn btn-outline-success btn-icon mg-r-5"'),
            'link_print' => anchor($this->tabel . '/action/add', 'Print', 'class="btn btn-success btn-small hidden-phone"'));
        $this->load->view('templates', $data);
    }

    function searchdata() {
        $this->session->set_userdata('caridata', 'cr' . $this->tabel);
        $this->session->set_userdata('finds', $this->input->post('lcfinds'));
        $this->session->set_userdata('findt', $this->input->post('lcfindt'));
        redirect($this->tabel);
    }
    
    function action($para1 = '', $para2 = '') {
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titleket'] = $this->titleket;

        if ($para1 == 'save') {
            $lcid = $this->session->userdata('produkimage');
            $timeinsert = time();
            $datatabel = array(
                'nama' => $this->input->post('lcnama'),
                'urut' => $this->input->post('lcurut'),
                'id_produk' => $lcid,
                'insertdate' => $timeinsert,
                'userinsert' => $this->session->userdata('iduserlog')
            );
            $this->db->insert($this->tabel, $datatabel);
            $id = $this->db->insert_id();
            $path = $_FILES['img']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $data_banner['photo'] = 'listproduk_' . $id . '.' . $ext;
            $this->crudmodel->file_up("img", "listproduk", $id, '', 'no', '.' . $ext);
            $this->db->where('id_image_produk', $id);
            $this->db->update($this->tabel, $data_banner);
//            $this->crudmodel->set_category_data(0);
//            recache();
            redirect($this->tabel);
        } elseif ($para1 == 'update') {
            $lcnama = $this->input->post('lcnama');
            $hawal = strtoupper(substr($lcnama, 0,1));
            $lckodekatagori = $hawal;
            for( $x=1 ; $x < 5 ; $x++){
                $lckodekatagori .= $hawal;
            }
            $timeinsert = time();
            $datatabel = array(
                'nama' => $lcnama,
                'keterangan' => $this->input->post('lcketerangan'),
                'urut' => $this->input->post('lcurut'),
                'kode_katagori' => $lckodekatagori,
                'halaman' => $this->input->post('lchalaman'),
                'insertdate' => $timeinsert,
                'userinsert' => $this->session->userdata('iduserlog')
            );
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->update($this->tabel, $datatabel);
            if ($_FILES['img']['name'] !== '') {
                $path = $_FILES['img']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $data_logo['photo'] = 'katagori_' . $para2 . '.' . $ext;
                $this->crudmodel->file_up("img", "katagori", $para2, '', 'no', '.' . $ext);
                $this->db->where('id_katagori', $para2);
                $this->db->update($this->tabel, $data_logo);
            }
//            $this->crudmodel->set_category_data(0);
//            recache();
            redirect($this->tabel);
        } elseif ($para1 == 'add') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = $this->tabel . '/form';
            $data['form_action'] = site_url($this->tabel . '/action/save/');
        
            $lcid = $this->session->userdata('produkimage');
            $lclevel0 = $this->crudmodel->get_data_by_id($this->tabel, $lcid)->row(); 
            
//            $data['data']['lcid'] = $lcDataList->id_katagori;
            
            
        
//              $data['title'] = 'Sub '. $lclevel0->nama ;
            
            
            $this->load->view('tempfroms', $data);
        } elseif ($para1 == 'edit') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = 'sub_'.$this->tabel . '/form';
            $data['form_action'] = site_url('sub_'.$this->tabel . '/action/update/' . $para2);
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->get($this->tabel);
            $lcDataList = $this->crudmodel->get_data_by_id($this->tabel, $para2)->row();
            $data['data']['lcid'] = $lcDataList->id_katagori;
            $data['data']['lcnama'] = $lcDataList->nama;
            $data['data']['lcketerangan'] = $lcDataList->keterangan;
            $data['data']['lcurut'] = $lcDataList->urut;
            $data['data']['lcphoto'] = $lcDataList->photo;
            
            $listhalm = $this->db->get('halaman')->result();
            $num_row = $this->db->get('halaman')->num_rows();
            $data['listhalm'][0] = " Pilih Link Halaman ...";
            if ($num_row > 0) {
                foreach ($listhalm as $row) {
                    $data['listhalm'][$row->kode] = $row->kode;
                }
            }
            $this->load->view('tempfroms', $data);
        } elseif ($para1 = 'delete') {
            $data = array(
                'aktif' => 3,
                'editdate' => $timeinsert,
                'useredit' => $this->session->userdata('iduserlog'));
                $this->db->where('id_'.$this->tabel, $para2);
                $this->db->update($this->tabel, $data);
//            $this->db->where('id_' . $this->tabel, $para2);
//            $this->db->delete($this->tabel);
//            $this->crudmodel->set_category_data(0);
            redirect($this->tabel);
        }
    }
}
