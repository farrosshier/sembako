<?php

require APPPATH . '/libraries/REST_Controller.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UploadPhoto
 *
 * @author user
 */
class Uploadphoto extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->library('helper');
        $this->load->model('authmodel');
        $this->load->model('accountmodel');
        $this->load->model('imglib');
    }

    function index_post() {

        $clientid = $this->post('clientid');
        if (empty($clientid))
            $clientid = 0;

        // reading other post parameters
        $accountid = $this->post('accountid');
        if (empty($accountid))
            $accountid = '';

        $token = $this->post('token');
        if (empty($token))
            $token = '';

        $clientid = $this->helper->clearInt($clientid);
        $accountid = $this->helper->clearInt($accountid);
        $token = $this->helper->clearText($token);


        if ($clientid != CLIENT_ID) {

            $this->helpel->printError(ERROR_UNKNOWN, CLIENT_ID + " Error client Id.");
        }

        $access_data = $this->authmodel->tokentoid($token);

        if ($access_data["error"] === false) {
//

            if ($access_data['accountid'] == $accountid) {

                $uploaded_file = "";
                $uploaded_file_name = "";
                $uploaded_file_ext = "";

                if (isset($_FILES['uploaded_file']['name'])) {

                    $uploaded_file = $_FILES['uploaded_file']['tmp_name'];
                    $uploaded_file_name = basename($_FILES['uploaded_file']['name']);
                    $uploaded_file_ext = pathinfo($_FILES['uploaded_file']['name'], PATHINFO_EXTENSION);

                    try {

                        $time = time();
                        if (!move_uploaded_file($_FILES['uploaded_file']['tmp_name'], TEMP_PATH . "{$time}." . $uploaded_file_ext)) {

                            // make error flag true
                            $access_data['error'] = true;
                            $access_data['message'] = 'Could not move the file!';
                        }

//                $imglib = new imglib($dbo);
                        $access_data = $this->imglib->createPhoto(TEMP_PATH . "{$time}." . $uploaded_file_ext);
//                unset($imglib);

                        if ($access_data['error'] === false) {

//                    $account = new account($dbo, $accountId);
                            $this->accountmodel->setPhoto($accountid, $access_data);
                        }
                    } catch (Exception $e) {

                        // Exception occurred. Make error flag true
                        $access_data['error'] = true;
                        $access_data['message'] = $e->getMessage();
                    }
                } else {

                    $access_data = array("error" => true,
                        "error_code" => ERROR_UNKNOWN,
                        "error_type" => 1,
                        "error_description" => "Account Id error");
                }
            }

//        $result = array("status" => 200, "message" => "ancur ancuran ", "data" => $arre);
//        echo $result;
            $this->authmodel->show_response($access_data);

//        echo json_encode($access_data);
//        exit;
        }

        // Path to move uploaded files
    }

}

?>
