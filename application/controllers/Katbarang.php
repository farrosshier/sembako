<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Katbarang
 *
 * @author user
 */
class Katbarang extends CI_Controller{
    function __construct() {
        parent::__construct();
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }

    var $limit = 10;
    var $title = 'Kategori Barang';
    var $titleket = 'Kategori Barang';
    var $linkweb = 'Katbarang';
    var $titlemenu = 'Mitra';
    var $tabel = 'katbarang';

    function index() {
        $this->listdata();
    }

    function listdata($offset = 0) {
        ;
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titlemenu'] = $this->titlemenu;
        $data['main_view'] = 'tabel';
        $data['form_action'] = site_url($this->linkweb . '/searchdata');
        $data['search'] = array('kode_kurir' => 'Code',
            'nama' => 'Name',
            'alamat' => 'Address',
            'telp1' => 'Phone Number',
            'nomor_kend' => 'Nomor Kendaraan',
        );
        $data['sfocus'] = 'kode_kurir';
        $finds = $this->session->userdata('finds');
        $findt = $this->session->userdata('findt');

        if ($this->session->userdata('caridata') == 'cr' . $this->tabel) {
            $finds = $this->session->userdata('finds');
            $findt = $this->session->userdata('findt');
        } else {
            $finds = '';
            $findt = '';
        }
        $uri_segment = 3;
        $offset = $this->uri->segment($uri_segment);

        $mDatalist = $this->crudmodel->list_data($this->tabel, $this->limit, $offset, $finds, $findt)->result();
        $numRow = $this->crudmodel->count_data($this->tabel);
        if ($numRow > 0) {
            $config['base_url'] = site_url('kurir/listdata');
            $config['total_rows'] = $numRow;
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;
            $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
            $config['first_tag_open'] = ' <li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = '<i class="fa fa-angle-right"></i>';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item">';
            $config['cur_tag_close'] = '</li>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination'] = ' Total Record ' . $numRow . "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" . $this->pagination->create_links();
            $tmpl = array('table_open' => '<table class="table table-hover table-bordered mg-b-0">',
                'heading_row_start' => '<thead class="bg-info"><tr>',
                'heading_row_end' => '</tr></thead>',
                'heading_cell_start' => '<th>',
                'heading_cell_end' => '</th>',
                'row_start' => '<tr>',
                'row_end' => '</tr>',
                'row_alt_start' => '<tr>',
                'row_alt_end' => '</tr>'
            );
            $this->table->set_template($tmpl);
            $this->table->set_heading(
                    array('data' => 'No', 'style' => 'width:3%'), 
                    array('data' => 'Photo', 'style' => 'width:7%'), 
                    array('data' => 'Kode', 'style' => 'width:4%'), 
                    array('data' => 'Nama', 'style' => 'width:20%'), 
                    array('data' => 'level', 'style' => 'width:40%'), 
                     
                    array('data' => '', 'style' => 'width:7%'));
            $i = 0 + $offset;
            foreach ($mDatalist as $sDataList) {
                if ($this->session->userdata('access') == '257') {
                    $this->table->add_row(++$i, '<img src="' . base_url() . 'uploads/category_image/' . $sDataList->logo . '" class="wd-40" alt="Image">', anchor($this->tabel . '/action/edit/' . $sDataList->id_katbarang, $sDataList->kode_barang), $sDataList->nama, $sDataList->level, 
                            anchor($this->tabel . '/action/edit/' . $sDataList->id_katbarang, '<i class="icon ion-edit"></i>', array('class' => "edit-row", 'data-original-title' => 'Edit')) . "&nbsp&nbsp&nbsp" .
                            anchor($this->tabel . '/action/delete/' . $sDataList->id_katbarang, '<i class="icon ion-trash-a"></i>', array('class' => "delete-row", 'data-original-title' => 'Delete', 'onclick' => "return confirm('Anda yakin akan menghapus data ini?')"))
                    );
                } else if ($this->session->userdata('access') == '100') {
                    $this->table->add_row(++$i, '<img src="' . base_url() . 'uploads/catagory_image/' . $sDataList->logo . '" class="wd-40" alt="Image">', $sDataList->kode_barang, $sDataList->nama, $sDataList->level
                    );
                }
            }

            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data !';
        }
        $data['link'] = array('link_add' => anchor($this->tabel . '/action/add', '<div><i class="fa fa-plus"></i></div>', 'class="btn btn-outline-success btn-icon mg-r-5"'),
            'link_print' => anchor($this->tabel . '/action/add', 'Print', 'class="btn btn-success btn-small hidden-phone"'));
        $this->load->view('templates', $data);
    }

    function searchdata() {
        $this->session->set_userdata('caridata', 'cr' . $this->tabel);
        $this->session->set_userdata('finds', $this->input->post('lcfinds'));
        $this->session->set_userdata('findt', $this->input->post('lcfindt'));
        redirect($this->tabel);
    }

    function action($para1 = '', $para2 = '') {
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titleket'] = $this->titleket;

        if ($para1 == 'save') {
           $lcnama = $this->input->post('lcnama');
           $lchAwal = substr($lcnama , 1, 1);
           for ($i=0 ; $i<5 ;$i++){
               $lchAwal .= $lchAwal;
           }
            
            $datatabel = array(
                'nama' => $this->input->post('lcnama'),
                'kode_barang' => $lchAwal,
                'lavel' => 1,
              
            );
            $this->db->insert($this->tabel, $datatabel);
            $id = $this->db->insert_id();
            $path = $_FILES['img']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $data_banner['logo'] = 'category_' . $id . '.' . $ext;
            $this->crudmodel->file_up("img", "category", $id, '', 'no', '.' . $ext);
            $this->db->where('id_katbarang', $id);
            $this->db->update($this->tabel, $data_banner);
//            $this->crudmodel->set_category_data(0);
//            recache();
            redirect($this->tabel);
        } elseif ($para1 == 'update') {
            $salt = $this->authmodel->generateSalt(10);
            $timeinsert = time();
            $enscript1 = "AggOpDSU!";
            $enscript2 = "!Machfudh...?";
            $password = $this->input->post('lcuserpass');
            $pass = md5($enscript1 . md5($password) . $enscript2);
            $passw_hash = md5(md5($pass) . $salt);
            if ($this->input->post('lcuserpass') == "" || $this->input->post('lcuserpass') == null) {
                $data = array(
                    'no_anggota' => $this->input->post('lcno_anggota'),
                    'nama' => $this->input->post('lcnama'),
                    'alamat' => $this->input->post('lcalamat'),
                    'no_hp' => $this->input->post('lcno_hp'),
                    'temp_lahir' => $this->input->post('lctemp_lahir'),
                    'tgl_lahir' => $this->input->post('lctgl_lahir'),
                    'jekel' => $this->input->post('lcjekel'),
                    'kode_jabatan' => $this->input->post('lckode_jabatan'),
                    'ket' => $this->input->post('lcketerangan'),
                    'userlogin' => $this->input->post('lcuserlogin'),
                    'salt' => $salt,
                    'tgl_edit' => $timeinsert,
                    'useredit' => $this->session->userdata('iduserlog'),
                );
            } else {
                $data = array(
                    'no_anggota' => $this->input->post('lcno_anggota'),
                    'nama' => $this->input->post('lcnama'),
                    'alamat' => $this->input->post('lcalamat'),
                    'no_hp' => $this->input->post('lcno_hp'),
                    'temp_lahir' => $this->input->post('lctemp_lahir'),
                    'tgl_lahir' => $this->input->post('lctgl_lahir'),
                    'jekel' => $this->input->post('lcjekel'),
                    'kode_jabatan' => $this->input->post('lckode_jabatan'),
                    'ket' => $this->input->post('lcketerangan'),
                    'userlogin' => $this->input->post('lcuserlogin'),
                    'userpass' => $passw_hash,
                    'salt' => $salt,
                    'tgl_edit' => $timeinsert,
                    'useredit' => $this->session->userdata('iduserlog'),
                );
            }
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->update($this->tabel, $data);
            if ($_FILES['img']['name'] !== '') {
                $path = $_FILES['img']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $data_logo['photoanggota'] = 'anggota_' . $para2 . '.' . $ext;
                $this->crudmodel->file_up("img", "anggota", $para2, '', 'no', '.' . $ext);
                $this->db->where('id_anggota', $para2);
                $this->db->update($this->tabel, $data_logo);
            }
//            $this->crudmodel->set_category_data(0);
//            recache();
            redirect($this->tabel);
        } elseif ($para1 == 'add') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = $this->tabel . '/form';
            $data['form_action'] = site_url($this->tabel . '/action/save/');
           
            $this->load->view('tempfroms', $data);
        } elseif ($para1 == 'edit') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = $this->tabel . '/formedit';
            $data['form_action'] = site_url($this->tabel . '/action/update/' . $para2);
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->get($this->tabel);
            $lcDataList = $this->crudmodel->get_data_by_id($this->tabel, $para2)->row();
            $data['data']['lcid'] = $lcDataList->id_anggota;
            $data['data']['lcno_anggota'] = $lcDataList->no_anggota;
            $data['data']['lcnama'] = $lcDataList->nama;
            $data['data']['lcalamat'] = $lcDataList->alamat;
            $data['data']['lcno_hp'] = $lcDataList->no_hp;
            $data['data']['lctemp_lahir'] = $lcDataList->temp_lahir;
            $data['data']['lctgl_lahir'] = $lcDataList->tgl_lahir;
            $data['data']['lcjekel'] = $lcDataList->jekel;
            $data['data']['lckode_jabatan'] = $lcDataList->kode_jabatan;
            $data['data']['lcketerangan'] = $lcDataList->ket;
            $data['data']['lcuserlogin'] = $lcDataList->userlogin;
            $data['data']['lcuserpass'] = "";
            $data['data']['lcphoto'] = $lcDataList->photoanggota;
            
            $lsjab = $this->db->get('jabatan')->result();
            $num_row = $this->db->get('jabatan')->num_rows();
            $data['listjab'][0] = " Pilih Jabatan ...";
            if ($num_row > 0) {
                foreach ($lsjab as $row) {
                    $data['listjab'][$row->id_jabatan] = $row->nama;
                }
            }
            $this->load->view('tempfroms', $data);
        } elseif ($para1 = 'delete') {
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->delete($this->tabel);
//            $this->crudmodel->set_category_data(0);
            redirect($this->tabel);
        }
    }
}
