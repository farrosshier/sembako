<?php

require APPPATH . '/libraries/REST_Controller.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Submitorderdetailandr
 *
 * @author user
 */
class Submitorderdetailandr extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }

    function index_post() {
        //put your code here


        $data = json_decode(file_get_contents("php://input"), true);

        $clientid = $data['clientid'];
        $token = $data['token'];


        if ($clientid != CLIENT_ID) {

            $this->helpel->printError(ERROR_UNKNOWN, CLIENT_ID + " Error client Id.");
        }

        $access_data = array();

//        $access_data = array(
//            'status' => 'success',
//            'msg' => 'dudul bener',
//            'accountid' => 2
//        );

        $access_data = $this->authmodel->tokentoid($token);

//        $result = array("status" => 404, "message" => FALSE, "kurir" => $access_data);
        
//        $access_data = $this->accountmodel->insertlistorder($data['product_order_detail']);

        if ($access_data["error"] === false) {
        
            $access_data = $this->accountmodel->insertinvoice($access_data['accountid'],$data['product_order'],$data['product_order_detail'],"SL");
            
//            $access_data = $this->accountmodel->insertlistorder($data['product_order_detail'] );
            
            $invoiceno = $access_data['invoice_no'];
            $nilaitot = $access_data['totalbayar'];
            
            $access_data = array(
                'error' => false,
                'error_code' => "02",
                'accesstoken' => $token
            );
            $access_data['data'] = array(
                'id' => 1,
                'code' => $invoiceno,
                'bayar' => $nilaitot
                
            ); // $this->accountmodel->getorder($access_data['accountid']);
        }else{
             $access_data = array(
                'error' => true,
                'error_code' => "02",
                'accesstoken' => $token
            );
        }

        $this->authmodel->show_response($access_data);
    }

}
