<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mitra
 *
 * @author user
 */
class Mitra extends CI_Controller{
   function __construct() {
        parent::__construct();
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }

    var $limit = 10;
    var $title = 'Mitra';
    var $titleket = 'Mitra';
    var $linkweb = 'Mitra';
    var $titlemenu = 'Master Data';
    var $tabel = 'mitra';

    function index() {
        $this->listdata();
    }

    function listdata($offset = 0) {
        ;
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titlemenu'] = $this->titlemenu;
        $data['main_view'] = 'tabel';
        $data['form_action'] = site_url($this->linkweb . '/searchdata');
        $data['search'] = array('kode_kurir' => 'Code',
            'nama' => 'Name',
            'alamat' => 'Address',
            'telp1' => 'Phone Number',
            'nomor_kend' => 'Nomor Kendaraan',
        );
        $data['sfocus'] = 'kode_kurir';
        $finds = $this->session->userdata('finds');
        $findt = $this->session->userdata('findt');
        $lavel = 1;

        if ($this->session->userdata('caridata') == 'cr' . $this->tabel) {
            $finds = $this->session->userdata('finds');
            $findt = $this->session->userdata('findt');
        } else {
            $finds = '';
            $findt = '';
        }
        $uri_segment = 3;
        $offset = $this->uri->segment($uri_segment);

        $mDatalist = $this->crudmodel->list_data($this->tabel, $this->limit, $offset, $finds, $findt)->result();
        $numRow = $this->crudmodel->count_data($this->tabel);
        if ($numRow > 0) {
            $config['base_url'] = site_url('kurir/listdata');
            $config['total_rows'] = $numRow;
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;
            $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
            $config['first_tag_open'] = ' <li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = '<i class="fa fa-angle-right"></i>';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item">';
            $config['cur_tag_close'] = '</li>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination'] = ' Total Record ' . $numRow . "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" . $this->pagination->create_links();
            $tmpl = array('table_open' => '<table class="table table-hover table-bordered mg-b-0">',
                'heading_row_start' => '<thead class="bg-info"><tr>',
                'heading_row_end' => '</tr></thead>',
                'heading_cell_start' => '<th>',
                'heading_cell_end' => '</th>',
                'row_start' => '<tr>',
                'row_end' => '</tr>',
                'row_alt_start' => '<tr>',
                'row_alt_end' => '</tr>'
            );
            $this->table->set_template($tmpl);
            $this->table->set_heading(
                    array('data' => 'Index', 'style' => 'width:3%'), 
                    array('data' => 'Nama', 'style' => 'width:10%'), 
                    array('data' => 'Keterangan'),
                    array('data' => 'Area 1', 'style' => 'width:9%'),
                    array('data' => 'Area 2', 'style' => 'width:9%'),
                    array('data' => 'Area 3', 'style' => 'width:9%'),
                    array('data' => '', 'style' => 'width:10%'));
            $i = 0 + $offset;
            foreach ($mDatalist as $sDataList) {
                if ($this->session->userdata('access') == '257') {
                    $this->table->add_row(++$i, anchor($this->tabel . '/action/edit/' . $sDataList->id_mitra, $sDataList->nama),$sDataList->keterangan,$sDataList->area1,$sDataList->area2,$sDataList->area3,  
                            anchor($this->tabel . '/action/edit/' . $sDataList->id_mitra, '<i class="icon ion-edit"></i>', array('class' => "edit-row", 'data-original-title' => 'Edit')) . "&nbsp&nbsp&nbsp" .
                            anchor($this->tabel . '/action/delete/' . $sDataList->id_mitra, '<i class="icon ion-trash-a"></i>', array('class' => "delete-row", 'data-original-title' => 'Delete', 'onclick' => "return confirm('Anda yakin akan menghapus data ini?')"))
                    );
                } else if ($this->session->userdata('access') == '100') {
                    $this->table->add_row(++$i, '<img src="' . base_url() . 'uploads/katagori_image/' . $sDataList->photo . '" class="wd-40" alt="Image">', $sDataList->nama,$sDataList->keterangan, $sDataList->halaman
                    );
                }
            }

            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data !';
        }
        $data['link'] = array('link_add' => anchor($this->tabel . '/action/add', '<div><i class="fa fa-plus"></i></div>', 'class="btn btn-outline-success btn-icon mg-r-5"'),
            'link_print' => anchor($this->tabel . '/action/add', 'Print', 'class="btn btn-success btn-small hidden-phone"'));
        $this->load->view('templates', $data);
    }

    function searchdata() {
        $this->session->set_userdata('caridata', 'cr' . $this->tabel);
        $this->session->set_userdata('finds', $this->input->post('lcfinds'));
        $this->session->set_userdata('findt', $this->input->post('lcfindt'));
        redirect($this->tabel);
    }
    
    function subkatagori($id) {
//        $this->db->where('id_'.$this->tabel,$id);
//        $lclevel = $this->db->get($this->tabel);  
//        $lclevel = $this->crudmodel->get_data_by_id($this->tabel, $id)->row();
        $this->session->set_userdata('subkatagori', $id);
        redirect('sub_katagori');
    }

    function action($para1 = '', $para2 = '') {
//        $data['menu'] = $this->Callmenu->menu();
        $data['title'] = $this->title;
        $data['titleket'] = $this->titleket;

        if ($para1 == 'save') {
            $lcnama = $this->input->post('lcnama');
            $timeinsert = time();
            $datatabel = array(
                'nama' => $lcnama,
                'keterangan' => $this->input->post('lcketerangan'),
                'area1' => $this->input->post('lcarea1'),
                'area2' => $this->input->post('lcarea2'),
                'area3' => $this->input->post('lcarea3'),
                'alamat' => $this->input->post('lcalamat'),
                'insertdate' => $timeinsert,
                'userinsert' => $this->session->userdata('iduserlog')
            );
            $this->db->insert($this->tabel, $datatabel);
            redirect($this->tabel);
        } elseif ($para1 == 'update') {
            $lcnama = $this->input->post('lcnama');
            $timeinsert = time();
            $datatabel = array(
                'nama' => $lcnama,
                'keterangan' => $this->input->post('lcketerangan'),
                'area1' => $this->input->post('lcarea1'),
                'area2' => $this->input->post('lcarea2'),
                'area3' => $this->input->post('lcarea3'),
                'alamat' => $this->input->post('lcalamat'),
                'editdate' => $timeinsert,
                'useredit' => $this->session->userdata('iduserlog')
            );
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->update($this->tabel, $datatabel);
            redirect($this->tabel);
        } elseif ($para1 == 'add') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = $this->tabel . '/form';
            $data['form_action'] = site_url($this->tabel . '/action/save/');
           
            $this->load->view('tempfroms', $data);
        } elseif ($para1 == 'edit') {
            $data['titlemenu'] = $this->titlemenu;
            $data['main_view'] = $this->tabel . '/form';
            $data['form_action'] = site_url($this->tabel . '/action/update/' . $para2);
            $this->db->where('id_' . $this->tabel, $para2);
            $this->db->get($this->tabel);
            $lcDataList = $this->crudmodel->get_data_by_id($this->tabel, $para2)->row();
            $data['data']['lcid'] = $lcDataList->id_katagori;
            $data['data']['lcnama'] = $lcDataList->nama;
            $data['data']['lcketerangan'] = $lcDataList->keterangan;
            $data['data']['lcarea1'] = $lcDataList->area1;
            $data['data']['lcarea2'] = $lcDataList->area2;
            $data['data']['lcarea3'] = $lcDataList->area3;
            $data['data']['lcalamat'] = $lcDataList->alamat;
            $this->load->view('tempfroms', $data);
        } elseif ($para1 = 'delete') {
            $data = array(
                'aktif' => 3,
                'editdate' => $timeinsert,
                'useredit' => $this->session->userdata('iduserlog'));
                $this->db->where('id_'.$this->tabel, $para2);
                $this->db->update($this->tabel, $data);
//            $this->db->where('id_' . $this->tabel, $para2);
//            $this->db->delete($this->tabel);
//            $this->crudmodel->set_category_data(0);
            redirect($this->tabel);
        }
    }
}
