<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pinjam
 *
 * @author user
 */
class Pinjam extends CI_Controller{
    function __construct() {
        parent::__construct();
//        if ($this->session->userdata('username') == null) {
//            redirect('login');
//        }
        $this->load->model("crudmodel");
        $this->load->model("authmodel");
    }

    var $limit = 10;
    var $datainsert = 0;
    var $title = 'Anggota';
    var $titleket = 'Pengajuan Pinjaman Anggota';
    var $linkweb = 'Anggota';
    var $titlemenu = 'Data';
    var $tabel = 'pinjam';

    function index() {
        $data['title'] = $this->title;
        $data['titleket'] = $this->titleket;
        $data['titlemenu'] = $this->titlemenu;
        $data['main_view'] = $this->tabel . '/form';
        $data['form_action'] = site_url($this->tabel . '/scandata');
        
        
//        $this->session->set_userdata('jumlahdata', 0 );
//        
//        $this->session->set_userdata('datakurir', 0 );
//        
//        $lskat = $this->db->get('kurir')->result();
//        $num_row = $this->db->get('kurir')->num_rows();
//        $data['listkurir'][0] = " Pilih Kurir ...";
//        if ($num_row > 0) {
//            foreach ($lskat as $row) {
//                $data['listkurir'][$row->id_kurir] = $row->kode_kurir . " - " . $row->nama;
//            }
//        }
//
//        $datate = ''; 
//        $datarec = array();
//        $dataview = array();
//        
//        $this->session->set_userdata('jumlahdata',$datate); 
//        $this->session->set_userdata('jumlahrec',$datarec );
//        $this->session->set_userdata('viewdata',$dataview );
        
        $this->load->view('tempfroms', $data);
    }

    function scandata() {
        $data['title'] = $this->title;
        $data['titleket'] = $this->titleket;
        $data['titlemenu'] = $this->titlemenu;
        $data['main_view'] = $this->tabel . '/formscan';
        $data['form_action'] = site_url($this->tabel . '/simpenarray');


        $kode_kurir = $this->input->post('lcnoanggota');

        $this->db->where('no_anggota', $kode_kurir);
        $this->db->limit(1);
        $xxDataList = $this->db->get('anggota');
//        $lcDataList = $xxDataList->row(); 
        
//        $lcDataList = $this->crudmodel->get_data_by_id('anggota', $kode_kurir)->row();

//        echo $kode_kurir;    
        if( $xxDataList->num_rows() > 0 ){
        $lcDataList = $xxDataList->row();
//        echo $lcDataList->no_anggota;
        $datakurir = array(
            'lcid' => $lcDataList->id_anggota,
            'lcnoanggota' =>  $lcDataList->no_anggota,
            'lcnama' => $lcDataList->nama,
            'lcalamat' => $lcDataList->alamat,
            'lcphotoanggota' => $lcDataList->photoanggota    
                
        );
        
        $this->session->set_userdata('datakurir', $datakurir );
        
         $lsjenis = $this->db->get('v_kategori_pinjam')->result();
            $num_row = $this->db->get('v_kategori_pinjam')->num_rows();
            $data['listjenis'][0] = " Pilih Jenis Pinjaman ...";
            if ($num_row > 0) {
                foreach ($lsjenis as $row) {
                    $data['listjenis'][$row->id_kategori_pinjam] = $row->nama;
                }
            }
     
        $data['data']['lcid'] = $lcDataList->id_anggota;
        $data['data']['lcnoanggota'] = $lcDataList->no_anggota;
        $data['data']['lcnama'] = $lcDataList->nama;
        $data['data']['lcalamat'] = $lcDataList->alamat;
        $data['data']['lcphotoanggota'] = $lcDataList->photoanggota;

        }else{
            redirect('simpan');
        }

        $this->load->view('tempfroms', $data);
    }

    function simpenarray() {
        $timeinsert = time();
        $datatabel = array(
                'id_anggota' => $this->input->post('lcid'),
                'id_kategori_pinjam' => $this->input->post('lckategori_pinjam'),
                'nilai_pinjam' => $this->input->post('lcnilai'),
                'ket' => $this->input->post('lcketerangan'),
                'tgl_pengajuan' => $timeinsert,
                'tgl_insert' => $timeinsert,
                'userinsert' => $this->session->userdata('iduserlog'),
            );
            $this->db->insert($this->tabel, $datatabel);

         redirect('pinjam');
    }

    function simpendata() {
        
//        $data['form_action'] = site_url($this->tabel . '/savearray');

        $dataisian = $this->input->post('lcseqno');
        $this->db->where('seqno', $dataisian);
        $barang = $this->db->get('barang');
        
        $viewkurir = '';
        $databarang = $barang->row();
        $isidata = $barang->num_rows();
        $arry = array();
        $arryview = array();
        $viewkurir = '';
        $datate = $this->session->userdata('jumlahdata');
        $datarec = $this->session->userdata('jumlahrec');
        $dataview = $this->session->userdata('viewdata');
        
        if ($isidata > 0) {
            
            if($databarang->id_stati == 0  && $databarang->id_statret == 0 ){
//        $arry = array();
//        $arryview = array();
//        $viewkurir = '';
//        $datate = $this->session->userdata('jumlahdata');
//        $datarec = $this->session->userdata('jumlahrec');
//        $dataview = $this->session->userdata('viewdata');
//        $datarec = $arry;
//        $isiarr = count($datarec);
//        echo $isiarr;
//        $hasil = null;
        if( count($datarec) == 0 ){
            
//            $data_bar['id_kurir'] = $this->session->userdata('datakurir')['lcid'];
//            $data_bar['waktu_ambil'] = time();
//            $data_bar['id_statusshipp'] = 1 ;
//            $this->db->where('id_barang', $databarang->id_barang);
//            $this->db->update('barang', $data_bar);
            
//            echo "Masih Kosong ".$datarec;
            array_push($arry, $databarang->id_barang);
            array_push($arryview, "#".$databarang->seqno ."#".$databarang->nama);
            $datarec = $arry;
            $dataview = $arryview;
//            print_r($arry);
            $viewkurirx = $dataview[0];
            $datate = $datate + 1 ; 
            $this->session->set_userdata('jumlahdata', $datate ); 
            $this->session->set_userdata('jumlahrec', $datarec );
            $this->session->set_userdata('viewdata', $dataview );
            
        }else{
//            echo "Sudah Di isi  ".$datarec;
            $arry = $datarec; 
            $arryview = $dataview;
        
            $hasil = array_search($databarang->id_barang,$arry);
//            echo $hasil;
//        }
//        
        if( $hasil == null ){
//            
//            $data_bar['id_kurir'] = $this->session->userdata('datakurir')['lcid'];
//            $data_bar['waktu_ambil'] = time();
//            $data_bar['id_statusshipp'] = 1 ;
//            $this->db->where('id_barang', $databarang->id_barang);
//            $this->db->update('barang', $data_bar);
//            
//            }
        
//           if ($hasil != 0) {  
            array_push($arry, $databarang->id_barang);
            array_push($arryview, "#".$databarang->seqno ."#".$databarang->nama);
            $datarec = $arry;
            $dataview = $arryview;
//            print_r($arry);
//            $viewkurirx = $dataview[0];
            
//            $datarec = $arry;
//            print_r($arry);
            
            $datate = $datate + 1 ; 
            $this->session->set_userdata('jumlahdata', $datate ); 
            $this->session->set_userdata('jumlahrec', $datarec );
            $this->session->set_userdata('viewdata', $dataview );
            $viewkurirx = '';
            $i=0;        
//            for( $i=0; $i < count($datarec);$i++){
//               $viewkurirx .= $datarec[$i] ;
//            }

        }   
        $viewkurirx = '';
//        for( $i=0; $i < count($datarec);$i++){
//               $viewkurirx .= $dataview[$i]+"<br/>" ;
//            }
        
            }
        }   
        }
        $viewkurir .= '<div class="mail-item d-flex pd-y-10 pd-x-20">';
        $viewkurir .= '<div class="mg-l-30">';
        $viewkurir .= '    <h3 class="tx-14"> Total Data : ' . $this->session->userdata('jumlahdata') . '</h3>';
        $datarec = $this->session->userdata('jumlahrec');
//        foreach ( $datarec as $row ){
        if(count($datarec) > 0) {
        for( $i=0; $i < count($datarec);$i++){
            $viewkurir .= '    <h6 class="tx-14"><a href="" class="tx-inverse"> ' . $dataview[$i] . ' </a></h6>';
//            $viewkurirx .= $dataview[$i]+"<br/>" ;
            }
        }  
//        $viewkurir .= '    <h6 class="tx-14"><a href="" class="tx-inverse"> ' . $viewkurirx . ' </a></h6>';
//        }
//        $viewkurir .= '    <p class="tx-13 mg-b-10">' . $databarang->alamat1 .$data_bar['id_kurir']. '</p>';
        $viewkurir .= '</div>';
        $viewkurir .= '</div>';
        

        echo $viewkurir;
    }
    
    function savearray(){
        $datarec = $this->session->userdata('jumlahrec');
        
        if (count($datarec) > 0){
            for( $i=0; $i < count($datarec);$i++){
                
            $data_bar['id_kurir'] = $this->session->userdata('datakurir')['lcid'];
            $data_bar['waktu_ambil'] = time();
            $data_bar['id_statusshipp'] = 1 ;
            $this->db->where('id_barang', $datarec[$i]);
            $this->db->update('barang', $data_bar);
            }
        }
        
        redirect($this->tabel);
    }
    
    function getketpinjam() {
        $kode_kurir = $_POST['prov'];
        $viewkurir = '';
        if ($kode_kurir != 0) {
            $this->db->where('id_kategori_pinjam', $kode_kurir);
            $xxkurir = $this->db->get('v_kategori_pinjam');
            
            if($xxkurir->num_rows() > 0 ){
                $lskurir = $xxkurir->row();
                $nominal = $lskurir->nilai_pinjam; 
            if ($lskurir->id_kategori_pinjam != 0) {
                
                $viewkurir .= '<div class="row">';
                $viewkurir .= '<label class="col-sm-4 form-control-label">Nilai: <span class="tx-danger">*</span></label>';
                $viewkurir .= '<div class="col-sm-8 mg-t-10 mg-sm-t-0">';
                $viewkurir .= '    <input class="form-control" id="nama" name="lcnilai" type="text" placeholder="Nama"';
                $viewkurir .= '          value="'. $lskurir->nilai_pinjam . '">';
                $viewkurir .= '</div>';
                $viewkurir .= '</div><!-- row -->';
                
                $viewkurir .= '<div class="row">';
                $viewkurir .= '<label class="col-sm-4 form-control-label">keterangan: <span class="tx-danger">*</span></label>';
                $viewkurir .= '<div class="col-sm-8 mg-t-10 mg-sm-t-0">';
                $viewkurir .= '    <textarea rows="3" class="form-control" name="lcketerangan"  placeholder="Keterangan"></textarea>';
                $viewkurir .= '</div>';
                $viewkurir .= '</div><!-- row -->';
                
                
            }
        }
        }
        echo $viewkurir;
    }
    
    
}
