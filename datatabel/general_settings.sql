-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2018 at 10:33 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jualonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `general_settings_id` int(11) NOT NULL,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`general_settings_id`, `type`, `value`) VALUES
(1, 'system_name', 'dzds'),
(2, 'system_email', 'admin@shop.com'),
(3, 'system_title', 'dzds'),
(4, 'address', ''),
(5, 'phone', ''),
(6, 'language', 'english'),
(9, 'terms_conditions', '<p>terms and conditions</p>'),
(10, 'fb_appid', ''),
(11, 'fb_secret', ''),
(12, 'google_languages', '{}'),
(24, 'meta_description', ''),
(25, 'meta_keywords', ''),
(26, 'meta_author', 'ActiveItZone'),
(27, 'captcha_public', '6LdsXPQSAAAAALRQB-m8Irt6-2_s2t10QsVnndVN'),
(28, 'captcha_private', '6LdsXPQSAAAAAFEnxFqW9qkEU_vozvDvJFV67yho'),
(29, 'application_name', ''),
(30, 'client_id', ''),
(31, 'client_secret', ''),
(32, 'redirect_uri', ''),
(33, 'api_key', ''),
(44, 'contact_about', '<p>about contact</p>'),
(39, 'contact_phone', '00-000-00000'),
(40, 'contact_email', 'yourmail@mail.com'),
(41, 'contact_website', 'www.yoursite.com'),
(42, 'footer_text', '<p>Your Footer Text</p>'),
(43, 'footer_category', '["1","4","5","12"]'),
(38, 'contact_address', 'Demo Address'),
(45, 'admin_notification_sound', 'ok'),
(46, 'admin_notification_volume', '7.47'),
(47, 'privacy_policy', '<p>Privacy Policy</p>'),
(48, 'discus_id', ''),
(49, 'home_notification_sound', 'ok'),
(50, 'homepage_notification_volume', '7.36'),
(51, 'fb_login_set', 'no'),
(52, 'g_login_set', 'no'),
(53, 'slider', 'no'),
(54, 'revisit_after', '2'),
(55, 'default_member_product_limit', '5'),
(56, 'fb_comment_api', ''),
(57, 'comment_type', 'google'),
(58, 'vendor_system', 'ok'),
(59, 'cache_time', '1440'),
(60, 'file_folder', 'jfkfkiriwnfjkmskdcsdfasaa'),
(62, 'slides', 'ok'),
(63, 'preloader', '13'),
(64, 'preloader_bg', 'rgba(74,0,94,1)'),
(65, 'preloader_obj', 'rgba(255,255,255,1)'),
(66, 'contact_lat_lang', '(40.7127837, -74.00594130000002)'),
(67, 'google_api_key', ''),
(68, 'physical_product_activation', 'ok'),
(69, 'digital_product_activation', 'ok'),
(70, 'data_all_brands', '41:::Chevrolet;;;;;;40:::Ford;;;;;;39:::Nissan;;;;;;38:::Audi;;;;;;44:::Hyundai;;;;;;45:::BMW;;;;;;46:::Marcedes-Benz;;;;;;47:::Mitsubishi;;;;;;51:::Toyota;;;;;;52:::Honda;;;;;;54:::Volvo;;;;;;50:::Lamborghini;;;;;;55:::Porsche;;;;;;48:::Suzuki;;;;;;56:::Dunlop;;;;;;57:::Yamaha;;;;;;8:::Lucky Brand;;;;;;10:::Victoria''s Secret;;;;;;11:::Dior;;;;;;13:::Priscess Purse;;;;;;14:::En''or;;;;;;15:::Jlo;;;;;;9:::The Crystal Bride;;;;;;22:::Aigner;;;;;;25:::Hudson;;;;;;26:::Omega;;;;;;27:::Breitling;;;;;;30:::Giorgio Armani;;;;;;17:::Polo;;;;;;23:::Adidas;;;;;;24:::Asics;;;;;;33:::Cognac;;;;;;35:::Nike;;;;;;20:::Baume & Mercier;;;;;;21:::Pepe Jeans;;;;;;31:::Castillo;;;;;;37:::Puma;;;;;;18:::Rolex;;;;;;29:::Axe;;;;;;34:::Project Vision;;;;;;63:::Apple;;;;;;6:::Brighton;;;;;;7:::Tanishq;;;;;;16:::The Vested Interest;;;;;;12:::Bucket Feet;;;;;;19:::Elle;;;;;;100:::Gucci;;;;;;101:::eternal love;;;;;;102:::Calvin Klein'),
(71, 'data_all_vendors', '1:::Lavinia Mckee;;;;;;3:::Tom;;;;;;4:::Paprocki;;;;;;5:::Youn');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
