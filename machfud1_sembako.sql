-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 09, 2018 at 04:14 PM
-- Server version: 10.2.18-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `machfud1_sembako`
--

-- --------------------------------------------------------

--
-- Table structure for table `alamat`
--

CREATE TABLE `alamat` (
  `id_alamat` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jalan` varchar(50) NOT NULL,
  `no_rumah` varchar(4) NOT NULL,
  `kota` int(11) NOT NULL,
  `kecamatan` int(11) NOT NULL,
  `kelurahan` int(11) NOT NULL,
  `kode` varchar(1) NOT NULL,
  `longti` decimal(16,10) NOT NULL,
  `latitut` decimal(16,10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alamat`
--

INSERT INTO `alamat` (`id_alamat`, `id_anggota`, `nama`, `jalan`, `no_rumah`, `kota`, `kecamatan`, `kelurahan`, `kode`, `longti`, `latitut`) VALUES
(1, 2, 'Moh. Machfudh', 'Jl. Sukamulya 7', '15', 3, 1, 1, '1', '1.1124414000', '3.9237500000');

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(11) NOT NULL,
  `no_anggota` int(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `ket` varchar(200) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `userlogin` varchar(30) NOT NULL,
  `userpass` varchar(45) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `kode_status` int(11) NOT NULL,
  `kode_jabatan` int(11) NOT NULL,
  `originPhotoUrl` varchar(30) NOT NULL,
  `normalPhotoUrl` varchar(30) NOT NULL,
  `bigPhotoUrl` varchar(30) NOT NULL,
  `lowPhotoUrl` varchar(30) NOT NULL,
  `photoanggota` varchar(100) NOT NULL,
  `temp_lahir` varchar(50) NOT NULL,
  `jekel` varchar(1) NOT NULL DEFAULT '1',
  `salt` varchar(10) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `tgl_lahir` varchar(20) NOT NULL,
  `token` varchar(70) NOT NULL,
  `new_token` varchar(70) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `activedate` varchar(20) NOT NULL,
  `editdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `no_anggota`, `nama`, `ket`, `telp`, `userlogin`, `userpass`, `status`, `kode_status`, `kode_jabatan`, `originPhotoUrl`, `normalPhotoUrl`, `bigPhotoUrl`, `lowPhotoUrl`, `photoanggota`, `temp_lahir`, `jekel`, `salt`, `alamat`, `tgl_lahir`, `token`, `new_token`, `insertdate`, `activedate`, `editdate`) VALUES
(1, 222222, 'Siti Latifah', '', '222222222', 'latifah@gmail.com', '4ba7f4166c3cd9920dc8cb33b066d794', '1', 0, 0, '', '', '', 'thumb_low_7b28628', '', '', '1', 'wzo*a,,p,d', '', '', '6bb7a87cad5884a6e1abc155a5436d77fd5f4a8abe1953d5f27353523210bf32', '7079cf1d52aa56dcc1f69ff8a9d1a1bb541184ef985093cd4743905e69af4553', '1527918671', '1528533821', ''),
(2, 111111, 'Moh Machfudh', '', '081281119822', 'machfudh@gmail.com', 'c98e02cbec609e9923517245660513b7', '1', 0, 0, '', '', '', 'thumb_low_7b28628.jpg', '', '', '1', '731ww2876-', '', '', '13940aa63280846ccaf657de83b2850dcdc5c5c719f9c55d62484b24ad06e3bf', '516d6389e5b8461adb00c51d68ab1efa41283c6ed55178145cd91fdd69066e7a', '1528013588', '1541754647', '');

-- --------------------------------------------------------

--
-- Table structure for table `anggotalog`
--

CREATE TABLE `anggotalog` (
  `id_anggotalog` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `token` varchar(45) NOT NULL,
  `waktu` varchar(20) NOT NULL,
  `kegiatan` varchar(200) NOT NULL,
  `catatan` varchar(200) NOT NULL,
  `u_agent` text NOT NULL,
  `ip_addr` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggotalog`
--

INSERT INTO `anggotalog` (`id_anggotalog`, `id_anggota`, `client_id`, `token`, `waktu`, `kegiatan`, `catatan`, `u_agent`, `ip_addr`) VALUES
(0, 2, 2101, '13940aa63280846ccaf657de83b2850dcdc5c5c719f9c', '1533713452', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '36.69.219.33'),
(1, 1, 2101, '55f1a2f167d31a5f1225054ee8ae6438cf12d1b238fc7', '1527927364', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.8'),
(2, 2, 2101, 'df0b05176f6e63f6fd27ff79df8f104ff794da0552208', '1528013588', 'Register from Android', ' Register and login from android ', 'okhttp/3.3.1', '192.168.1.8'),
(3, 2, 2101, '8ec77512de173dd68354c26c0100a285acddb94b0f692', '1528013799', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.8'),
(4, 1, 2101, '6bb7a87cad5884a6e1abc155a5436d77fd5f4a8abe195', '1528015081', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.8'),
(5, 2, 2101, '50b716ef0ac6c6c89f135cb32b45641a6d6e1e64d34d9', '1530690878', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.0.105'),
(6, 2, 2101, '527fc240f85ca4c1c2121fe8571cd815bc5f5e3617a14', '1531122043', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(7, 2, 2101, '89d9b1f53bc9259fa3591f185ae466fe9f384d9c0ad7a', '1531219626', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(8, 2, 2101, '6dc484d4e1ee8f8fa22014f702eaa71043a20c7610f34', '1531288658', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(9, 2, 2101, 'e6b6809bea192e5755c1fe0bef56fd99894a01786ba59', '1531289588', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(10, 2, 2101, 'c8cd31819d93ae68de6cc308d4e4308bd5a8088da7da9', '1531293269', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(11, 2, 2101, '0140372a1a51e267b2a3ffa4922c36cecd29cc89331c1', '1531297028', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(12, 2, 2101, '5cf40a1a3b09b751906a81b9f147525823159505e7382', '1531310119', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(13, 2, 2101, '49788532ebf176c747cd26f73f15f999804867427ad59', '1531355834', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(14, 2, 2101, '802d5f179a16e21b371caee67ddcfbbd29379d28926a3', '1531377824', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(15, 2, 2101, '235f921963a49ab4378ee8cb0777949a48731f81d8877', '1531378284', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(16, 2, 2101, '3d82720feb1f128cd1aba5c0bb2bf5bf04505d06d5e74', '1531909778', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(17, 2, 2101, 'ca1340fbffdd859441e2406deba110d080667d8febc1d', '1531911221', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(18, 2, 2101, '8756a3113647ffb42afbf06ddabe61ceaf8406bbacec0', '1531911538', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(19, 2, 2101, '52b1b920f078b1113768a0713c354a272749ea3cebc33', '1531912625', 'Login from Android', ' Login from android ', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2386.0 Safari/537.36', '192.168.1.9'),
(20, 2, 2101, '1292e26ba1b1553a7accb22e696a22068ae838fde710d', '1531913046', 'Login from Android', ' Login from android ', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2386.0 Safari/537.36', '192.168.1.9'),
(21, 2, 2101, '26ca822ec7493c264e90ae5ff6126b0617703683b3c7b', '1531913310', 'Login from Android', ' Login from android ', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2386.0 Safari/537.36', '192.168.1.9'),
(22, 2, 2101, 'e6a0d8bcf5963a84f211d73c83aa249ac35ebf9750fb9', '1531913359', 'Login from Android', ' Login from android ', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2386.0 Safari/537.36', '192.168.1.9'),
(23, 2, 2101, 'c03ae67a27189ec545a7300be8ed0af86eb62b16de32d', '1531913402', 'Login from Android', ' Login from android ', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2386.0 Safari/537.36', '192.168.1.9'),
(24, 2, 2101, '3fc39b3049ff5b24303515782984ccbb9d92804d1119d', '1531913521', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(25, 2, 2101, '43f012508782270af520d40851e4027775de7a2245653', '1531915139', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(26, 2, 2101, '31e36087c165c662b83b518ca4e753b24ba9be8692fdd', '1532083318', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(27, 2, 2101, '07ddec00da1a9fdf37d4a636395b337e2ddc63e751114', '1532083595', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(28, 2, 2101, '077d03a8fdba3b043ad9a7eef7813044f21098d25622b', '1532083795', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(29, 2, 2101, 'd668a0effa08ec9fa70b60b2e1075105db61091a8fc25', '1532084025', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(30, 2, 2101, 'cfc5b111021d2f571719a2e48453477e3ca887539f292', '1532084287', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(31, 2, 2101, '21a8fa0b573af3f8297ecc523c7fb982cd83ece6cca0e', '1532084652', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(32, 2, 2101, 'c7f401e5cd0591a5e55914b48dc8fe1a5396190e218f6', '1532084904', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4'),
(33, 2, 2101, '9bcd21de63035524d29feee1f7306b26b7f10e33aaf92', '1532683225', 'Login from Android', ' Login from android ', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2386.0 Safari/537.36', '192.168.1.9'),
(34, 2, 2101, '8aa59080478fd059c95d234b323e0acf11c1d7cf79a87', '1532855649', 'Login from Android', ' Login from android ', 'okhttp/3.3.1', '192.168.1.4');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id_brand` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id_brand`, `nama`, `keterangan`, `photo`, `aktif`, `userinsert`, `insertdate`, `useredit`, `editdate`) VALUES
(1, 'ABC', 'Produk Abc', 'brand_1.jpg', '1', 1, '1527237807', 0, ''),
(2, 'Indofoot', 'Produk indo foot', 'brand_2.jpg', '1', 1, '1527237973', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `general_settings_id` int(11) NOT NULL,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`general_settings_id`, `type`, `value`) VALUES
(1, 'system_name', 'dzds'),
(2, 'system_email', 'admin@shop.com'),
(3, 'system_title', 'dzds'),
(4, 'address', ''),
(5, 'phone', ''),
(6, 'language', 'english'),
(9, 'terms_conditions', '<p>terms and conditions</p>'),
(10, 'fb_appid', ''),
(11, 'fb_secret', ''),
(12, 'google_languages', '{}'),
(24, 'meta_description', ''),
(25, 'meta_keywords', ''),
(26, 'meta_author', 'ActiveItZone'),
(27, 'captcha_public', '6LdsXPQSAAAAALRQB-m8Irt6-2_s2t10QsVnndVN'),
(28, 'captcha_private', '6LdsXPQSAAAAAFEnxFqW9qkEU_vozvDvJFV67yho'),
(29, 'application_name', ''),
(30, 'client_id', ''),
(31, 'client_secret', ''),
(32, 'redirect_uri', ''),
(33, 'api_key', ''),
(44, 'contact_about', '<p>about contact</p>'),
(39, 'contact_phone', '00-000-00000'),
(40, 'contact_email', 'yourmail@mail.com'),
(41, 'contact_website', 'www.yoursite.com'),
(42, 'footer_text', '<p>Your Footer Text</p>'),
(43, 'footer_category', '[\"1\",\"4\",\"5\",\"12\"]'),
(38, 'contact_address', 'Demo Address'),
(45, 'admin_notification_sound', 'ok'),
(46, 'admin_notification_volume', '7.47'),
(47, 'privacy_policy', '<p>Privacy Policy</p>'),
(48, 'discus_id', ''),
(49, 'home_notification_sound', 'ok'),
(50, 'homepage_notification_volume', '7.36'),
(51, 'fb_login_set', 'no'),
(52, 'g_login_set', 'no'),
(53, 'slider', 'no'),
(54, 'revisit_after', '2'),
(55, 'default_member_product_limit', '5'),
(56, 'fb_comment_api', ''),
(57, 'comment_type', 'google'),
(58, 'vendor_system', 'ok'),
(59, 'cache_time', '1440'),
(60, 'file_folder', 'jfkfkiriwnfjkmskdcsdfasaa'),
(62, 'slides', 'ok'),
(63, 'preloader', '13'),
(64, 'preloader_bg', 'rgba(74,0,94,1)'),
(65, 'preloader_obj', 'rgba(255,255,255,1)'),
(66, 'contact_lat_lang', '(40.7127837, -74.00594130000002)'),
(67, 'google_api_key', ''),
(68, 'physical_product_activation', 'ok'),
(69, 'digital_product_activation', 'ok'),
(70, 'data_all_brands', '41:::Chevrolet;;;;;;40:::Ford;;;;;;39:::Nissan;;;;;;38:::Audi;;;;;;44:::Hyundai;;;;;;45:::BMW;;;;;;46:::Marcedes-Benz;;;;;;47:::Mitsubishi;;;;;;51:::Toyota;;;;;;52:::Honda;;;;;;54:::Volvo;;;;;;50:::Lamborghini;;;;;;55:::Porsche;;;;;;48:::Suzuki;;;;;;56:::Dunlop;;;;;;57:::Yamaha;;;;;;8:::Lucky Brand;;;;;;10:::Victoria\'s Secret;;;;;;11:::Dior;;;;;;13:::Priscess Purse;;;;;;14:::En\'or;;;;;;15:::Jlo;;;;;;9:::The Crystal Bride;;;;;;22:::Aigner;;;;;;25:::Hudson;;;;;;26:::Omega;;;;;;27:::Breitling;;;;;;30:::Giorgio Armani;;;;;;17:::Polo;;;;;;23:::Adidas;;;;;;24:::Asics;;;;;;33:::Cognac;;;;;;35:::Nike;;;;;;20:::Baume & Mercier;;;;;;21:::Pepe Jeans;;;;;;31:::Castillo;;;;;;37:::Puma;;;;;;18:::Rolex;;;;;;29:::Axe;;;;;;34:::Project Vision;;;;;;63:::Apple;;;;;;6:::Brighton;;;;;;7:::Tanishq;;;;;;16:::The Vested Interest;;;;;;12:::Bucket Feet;;;;;;19:::Elle;;;;;;100:::Gucci;;;;;;101:::eternal love;;;;;;102:::Calvin Klein'),
(71, 'data_all_vendors', '1:::Lavinia Mckee;;;;;;3:::Tom;;;;;;4:::Paprocki;;;;;;5:::Youn');

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE `halaman` (
  `id_halaman` int(20) NOT NULL,
  `kode` varchar(4) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halaman`
--

INSERT INTO `halaman` (`id_halaman`, `kode`, `status`) VALUES
(1, 'SALE', '1'),
(2, 'LNRI', '1'),
(3, 'CCAC', '1'),
(4, 'PPOB', '1');

-- --------------------------------------------------------

--
-- Table structure for table `image_produk`
--

CREATE TABLE `image_produk` (
  `id_image_produk` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `urut` int(11) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_produk`
--

INSERT INTO `image_produk` (`id_image_produk`, `id_produk`, `nama`, `photo`, `urut`, `aktif`, `userinsert`, `insertdate`, `useredit`, `editdate`) VALUES
(1, 1, 'photo1', 'listproduk_1.jpg', 1, '1', 1, '1527390362', 0, ''),
(2, 1, 'photo 2', 'listproduk_2.jpg', 2, '1', 1, '1527390390', 0, ''),
(3, 4, 'Beras putih', 'listproduk_3.jpg', 1, '1', 1, '1530859884', 0, ''),
(4, 5, 'Beras Merah', 'listproduk_4.jpg', 1, '1', 1, '1530859932', 0, ''),
(5, 6, 'Beras Hitam', 'listproduk_5.jpg', 1, '1', 1, '1530862029', 0, ''),
(6, 6, 'Beras Dalam Kantong', 'listproduk_6.jpg', 2, '1', 1, '1530862059', 0, ''),
(7, 8, 'Gula ku', 'listproduk_7.png', 1, '1', 1, '1532876453', 0, ''),
(8, 8, 'Gula ku', 'listproduk_8.png', 2, '1', 1, '1532876469', 0, ''),
(9, 8, 'Gula ku', 'listproduk_9.png', 3, '1', 1, '1532876490', 0, ''),
(10, 9, 'Gula Pasir', 'listproduk_10.jpg', 1, '1', 1, '1532877684', 0, ''),
(11, 9, 'Gula Pasir', 'listproduk_11.jpg', 2, '1', 1, '1532877699', 0, ''),
(12, 9, 'Gula Pasir', 'listproduk_12.jpg', 3, '1', 1, '1532877717', 0, ''),
(13, 10, 'Telor', 'listproduk_13.jpeg', 1, '1', 1, '1532880400', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `invoiceorder`
--

CREATE TABLE `invoiceorder` (
  `id_invoiceorder` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `catatan` text NOT NULL,
  `invoice_no` varchar(20) NOT NULL,
  `invoice_date` varchar(20) NOT NULL,
  `id_pengiriman` int(11) NOT NULL,
  `id_shipping` int(11) NOT NULL,
  `shipping_date` varchar(20) NOT NULL,
  `shipping_status` int(11) NOT NULL,
  `shipping_ket` varchar(200) NOT NULL,
  `shipping_add` varchar(200) NOT NULL,
  `id_bank` int(11) NOT NULL,
  `vat` int(11) NOT NULL,
  `vat_persen` int(11) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `payment_detail` text NOT NULL,
  `payment_date` varchar(20) NOT NULL,
  `grand_total` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoiceorder`
--

INSERT INTO `invoiceorder` (`id_invoiceorder`, `id_anggota`, `nama`, `phone`, `catatan`, `invoice_no`, `invoice_date`, `id_pengiriman`, `id_shipping`, `shipping_date`, `shipping_status`, `shipping_ket`, `shipping_add`, `id_bank`, `vat`, `vat_persen`, `payment_type`, `payment_status`, `payment_detail`, `payment_date`, `grand_total`, `status`) VALUES
(0, 2, 'Moh Machfudh', '81281119822', '', 'SLGFW1535202146', '1535202146', 0, 0, '1535288535162', 0, 'Di Ambil', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 0, 0, '', '', 601000, 1),
(1, 2, 'Moh Machfudh', '81281119822', '', 'PRDdyg1532076301', '1532076301', 0, 0, '1532071542993', 1, 'Di Kirim', 'alamat', 1, 10, 0, 1, 1, 'zzzzzzzzzzzzzz', '1532691008', 0, 2),
(2, 2, 'Moh Machfudh', '81281119822', 'tesfgbvgv', 'S-0KO1532077752', '1532077752', 0, 0, '1532077735736', 1, 'Di Kirim', 'alamat', 1, 24, 0, 1, 1, 'xxxxx', '1532673975', 401100, 0),
(3, 2, 'Moh Machfudh', '81281119822', 'tesfgbvgv', 'S-J241532077889', '1532077889', 0, 0, '1532077735736', 1, 'Di Kirim', 'alamat', 0, 0, 0, 1, 1, '', '', 401100, 0),
(4, 2, 'Moh Machfudh', '81281119822', '+yytrrttg', 'S-ZXU1532078218', '1532078218', 0, 0, '1532078197404', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(5, 2, 'Moh Machfudh', '81281119822', 'xxxxxxzzzzzzz', 'S-*4Y1532084952', '1532084952', 0, 0, '1532084937969', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(6, 2, 'Moh Machfudh', '81281119822', 'xxxxxxzzzzzzz', 'S-A.-1532085589', '1532085589', 0, 0, '1532084937969', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(7, 2, 'Moh Machfudh', '81281119822', 'hhjkklllk', 'S-,A51532085884', '1532085884', 0, 0, '1532085864152', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(8, 2, 'Moh Machfudh', '81281119822', '@zzzzzzz', 'S-DW.1532086714', '1532086714', 0, 0, '1532086697642', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(9, 2, 'Moh Machfudh', '81281119822', '@zzzzzzz', 'S-GG01532088140', '1532088140', 0, 0, '1532086697642', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(10, 2, 'Moh Machfudh', '81281119822', '@zzzzzzz', 'S-QJ=1532088220', '1532088220', 0, 0, '1532086697642', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(11, 2, 'Moh Machfudh', '81281119822', '@zzzzzzz', 'S-SEL1532088272', '1532088272', 0, 0, '1532086697642', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(12, 2, 'Moh Machfudh', '81281119822', 'nnnnn', 'S-NR61532088592', '1532088592', 0, 0, '1532088575974', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(13, 2, 'Moh Machfudh', '81281119822', '', 'S-MHY1532177220', '1532177220', 0, 0, '1532177208289', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(14, 2, 'Moh Machfudh', '81281119822', '', 'S-QV01532177907', '1532177907', 0, 0, '1532177895781', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(15, 2, 'Moh Machfudh', '81281119822', 'rtyyh', 'S-RQA1532178466', '1532178466', 0, 0, '1532178449273', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(16, 2, 'Moh Machfudh', '81281119822', '', 'S--0I1532178699', '1532178699', 0, 0, '1532178684071', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(17, 2, 'Moh Machfudh', '81281119822', '', 'S-3_L1532178820', '1532178820', 0, 0, '1532178684071', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(18, 2, 'Moh Machfudh', '81281119822', '', 'S-L4B1532178873', '1532178873', 0, 0, '1532178684071', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(19, 2, 'Moh Machfudh', '81281119822', '', 'S-,2C1532178951', '1532178951', 0, 0, '1532178684071', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(20, 2, 'Moh Machfudh', '81281119822', '', 'S-K2X1532179390', '1532179390', 0, 0, '1532179372216', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(21, 2, 'Moh Machfudh', '81281119822', '', 'S-AGC1532179443', '1532179443', 0, 0, '1532179372216', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(22, 2, 'Moh Machfudh', '81281119822', '', 'S-4Q71532180588', '1532180588', 0, 0, '1532180571652', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(23, 2, 'Moh Machfudh', '81281119822', '', 'S-S-+1532182513', '1532182513', 0, 0, '1532182494629', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(24, 2, 'Moh Machfudh', '81281119822', '', 'S-1*V1532182846', '1532182846', 0, 0, '1532182808356', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(25, 2, 'Moh Machfudh', '81281119822', '', 'S-2PS1532183181', '1532183181', 0, 0, '1532183171013', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(26, 2, 'Moh Machfudh', '81281119822', '', 'S-15_1532183467', '1532183467', 0, 0, '1532183453232', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(27, 2, 'Moh Machfudh', '81281119822', '', 'S-G6X1532185454', '1532185454', 0, 0, '1532185443559', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(28, 2, 'Moh Machfudh', '81281119822', '', 'S-.771532231693', '1532231693', 0, 0, '1532231676963', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(29, 2, 'Moh Machfudh', '81281119822', '', 'S-F=Y1532232463', '1532232463', 0, 0, '1532231676963', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(30, 2, 'Moh Machfudh', '81281119822', '', 'S-O,Z1532232924', '1532232924', 0, 0, '1532231676963', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(31, 2, 'Moh Machfudh', '81281119822', '', 'S-T=P1532233233', '1532233233', 0, 0, '1532231676963', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(32, 2, 'Moh Machfudh', '81281119822', '', 'S-BUB1532233840', '1532233840', 0, 0, '1532231676963', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(33, 2, 'Moh Machfudh', '81281119822', '', 'S-0ZN1532233969', '1532233969', 0, 0, '1532231676963', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(34, 2, 'Moh Machfudh', '81281119822', '', 'S-KAO1532234404', '1532234404', 0, 0, '1532234387876', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(35, 2, 'Moh Machfudh', '81281119822', '', 'S-CZT1532234544', '1532234544', 0, 0, '1532234526342', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(36, 2, 'Moh Machfudh', '81281119822', '', 'S-ADX1532235335', '1532235335', 0, 0, '1532235321222', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(37, 2, 'Moh Machfudh', '81281119822', '', 'S-R3K1532235366', '1532235366', 0, 0, '1532235321222', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 0, 0, 0, 1, 1, '', '', 401100, 0),
(38, 2, 'Moh Machfudh', '81281119822', '', 'S-NQ11532236082', '1532236082', 0, 0, '1532236061612', 1, 'Di Kirim', 'Jl. Sukamulya 7 No.15, Jakarta Pusat\nKec : Gambir\nKel : Gambir 10110', 1, 2, 0, 1, 1, 'qqqqqqqqqqqqq', '1532691168', 401100, 2);

-- --------------------------------------------------------

--
-- Table structure for table `katagori`
--

CREATE TABLE `katagori` (
  `id_katagori` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `kode_katagori` varchar(5) NOT NULL,
  `halaman` varchar(5) NOT NULL,
  `urut` int(11) NOT NULL,
  `lavel` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `data_brand` varchar(200) NOT NULL,
  `data_vendor` varchar(200) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `katagori`
--

INSERT INTO `katagori` (`id_katagori`, `nama`, `keterangan`, `kode_katagori`, `halaman`, `urut`, `lavel`, `parent`, `data_brand`, `data_vendor`, `photo`, `aktif`, `userinsert`, `insertdate`, `useredit`, `editdate`) VALUES
(1, 'Sembako', 'Sembako', 'SSSSS', 'SALE', 1, 1, 0, '', '', 'katagori_1.png', '1', 1, '1530807578', 0, ''),
(2, 'LONDRY', 'Londri ', 'LLLLL', 'LNRI', 2, 1, 0, '', '', 'katagori_2.png', '1', 1, '1530806795', 0, ''),
(3, 'Sayuran', 'Sayuran', 'SSSSS', 'SALE', 1, 1, 0, '', '', 'katagori_3.jpg', '3', 1, '1527234028', 1, ''),
(4, 'Beras', 'Beras', 'BBBBB', '0', 1, 2, 1, '', '', 'katagori_4.jpg', '3', 1, '1532859003', 1, ''),
(5, 'Umum', 'umum', 'LUUUU', 'LNRI', 1, 2, 2, '', '', 'katagori_5.jpg', '1', 1, '1527235069', 0, ''),
(6, 'Gula Pasir', 'Gula Pasir', 'GGGGG', 'SALE', 2, 2, 1, '', '', 'katagori_6.jpg', '3', 1, '1532851221', 1, ''),
(7, 'Asem', 'Asem', 'DSAAA', 'SALE', 1, 3, 4, '', '', 'katagori_7.jpg', '1', 1, '1527235785', 0, ''),
(8, 'Buah buah', 'buah buahan', 'BBBBB', 'SALE', 3, 1, 0, '', '', 'katagori_8.png', '1', 1, '1530808488', 0, ''),
(9, 'Cuci AC', 'Cuci AC', 'CCCCC', 'CCAC', 4, 1, 0, '', '', 'katagori_9.png', '1', 1, '1530808078', 0, ''),
(10, 'Bersih bersih', 'Bersih Bersih', 'BBBBB', 'SALE', 5, 1, 0, '', '', 'katagori_10.png', '1', 1, '1533713816', 0, ''),
(11, 'Kirim Kirim', 'Kirim Kirim', 'KKKKK', 'SALE', 6, 1, 0, '', '', 'katagori_11.png', '1', 1, '1533713947', 0, ''),
(12, 'Antar Jemput', 'Antar Jemput', 'AAAAA', 'SALE', 7, 1, 0, '', '', 'katagori_12.png', '1', 1, '1533713797', 0, ''),
(13, 'Telor', 'Telor Aja deh', 'STTTT', '0', 3, 2, 1, '', '', 'katagori_13.', '1', 1, '1532880468', 0, ''),
(14, 'Beras', 'Beras', 'SBBBB', 'SALE', 1, 2, 1, '', '', 'katagori_14.', '1', 1, '1532861480', 0, ''),
(15, 'Gula Pasir', 'Gula Pasir', 'SGGGG', 'SALE', 2, 2, 1, '', '', 'katagori_15.', '1', 1, '1532861520', 0, ''),
(16, 'Lain Lain', 'Lain Lain', 'LLLLL', 'SALE', 8, 1, 0, '', '', 'katagori_0.png', '1', 1, '1533713895', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `namakec` varchar(30) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `id_kota`, `namakec`, `aktif`) VALUES
(1, 3, 'Gambir', '1'),
(2, 3, 'Tanah Abang', '1'),
(3, 3, 'Menteng', '1'),
(4, 3, 'Senen', '1'),
(5, 3, 'Cempaka Putih', '1'),
(6, 3, 'Johar Baru', '1'),
(7, 3, 'Kemayoran', '1'),
(8, 3, 'Sawah Besar', '1'),
(9, 4, 'Koja', '1'),
(10, 4, 'Kelapa Gading', '1'),
(11, 4, 'Tanjung Priok', '1'),
(12, 4, 'Pademangan', '1'),
(13, 4, 'Penjaringan', '1'),
(14, 4, 'Cilincing', '1'),
(15, 5, 'Matraman', '1'),
(16, 5, 'Pulo Gadung', '1'),
(17, 5, 'Jatinegara', '1'),
(18, 5, 'Duren Sawit', '1'),
(19, 5, 'Kramat Jati', '1'),
(20, 5, 'Makasar', '1'),
(21, 5, 'Pasar Rebo', '1'),
(22, 5, 'Ciracas', '1'),
(23, 5, 'Cipayung', '1'),
(24, 5, 'Cakung', '1'),
(25, 6, 'Kebayoran Baru', '1'),
(26, 6, 'Kebayoran Lama', '1'),
(27, 6, 'Pesanggrahan', '1'),
(28, 6, 'Cilandak', '1'),
(29, 6, 'Pasar Minggu', '1'),
(30, 6, 'Jagakarsa', '1'),
(31, 6, 'Mampang Prapatan', '1'),
(32, 6, 'Pancoran', '1'),
(33, 6, 'Tebet', '1'),
(34, 6, 'Setiabudi', '1'),
(35, 7, 'Cengkareng', '1'),
(36, 7, 'Grogol Petamburan', '1'),
(37, 7, 'Kalideres', '1'),
(38, 7, 'Kebon Jeruk', '1'),
(39, 7, 'Kembangan', '1'),
(40, 7, 'Palmerah', '1'),
(41, 7, 'Taman Sari', '1'),
(42, 7, 'Tambora', '1');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `namakel` varchar(30) NOT NULL,
  `kodepos` varchar(5) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id`, `id_kecamatan`, `namakel`, `kodepos`, `aktif`) VALUES
(1, 1, 'Gambir', '10110', '1'),
(2, 1, 'Kebon Kelapa', '10120', '1'),
(3, 1, 'Petojo Selatan', '10130', '1'),
(4, 1, 'Duri Pulo', '10140', '1'),
(5, 1, 'Cideng', '10150', '1'),
(6, 1, 'Petojo Utara', '10160', '1'),
(7, 2, 'Bendungan Hilir', '10210', '1'),
(8, 2, 'Karet Tengsin', '10220', '1'),
(9, 2, 'Kebon Melati', '10230', '1'),
(10, 2, 'Kebon Kacang', '10240', '1'),
(11, 2, 'Kampung Bali', '10250', '1'),
(12, 2, 'Petamburan', '10260', '1'),
(13, 2, 'Gelora', '10270', '1'),
(14, 3, 'Menteng', '10310', '1'),
(15, 3, 'Pegangsaan', '10320', '1'),
(16, 3, 'Cikini', '10330', '1'),
(17, 3, 'Kebon Sirih', '10340', '1'),
(18, 3, 'Gondangdia', '10350', '1'),
(19, 4, 'Senen', '10410', '1'),
(20, 4, 'Kwitang', '10420', '1'),
(21, 4, 'Kenari', '10430', '1'),
(22, 4, 'Paseban', '10440', '1'),
(23, 4, 'Kramat', '10450', '1'),
(24, 4, 'Bungur', '10460', '1');

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(11) NOT NULL,
  `namakota` varchar(30) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id`, `namakota`, `aktif`) VALUES
(3, 'Jakarta Pusat', '1'),
(4, 'Jakarta Utara', '1'),
(5, 'Jakarta Timur', '1'),
(6, 'Jakarta Selatan', '1'),
(7, 'Jakarta Barat', '1');

-- --------------------------------------------------------

--
-- Table structure for table `listorder`
--

CREATE TABLE `listorder` (
  `id_listorder` int(11) NOT NULL,
  `invoice_no` varchar(20) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `plu` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `diskon` double NOT NULL,
  `diskon_type` varchar(1) NOT NULL,
  `tax` double NOT NULL,
  `tax_type` varchar(1) NOT NULL,
  `harga` double NOT NULL,
  `qty` double NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listorder`
--

INSERT INTO `listorder` (`id_listorder`, `invoice_no`, `id_produk`, `plu`, `nama`, `diskon`, `diskon_type`, `tax`, `tax_type`, `harga`, `qty`, `photo`) VALUES
(0, 'SLGFW1535202146', 8, '', 'Gula pasir putih Gulaku 1 kg', 0, '', 0, '', 300000, 1, ''),
(1, '0', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(2, '0', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(3, 'S-J241532077889', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(4, 'S-J241532077889', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(5, 'S-ZXU1532078218', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(6, 'S-ZXU1532078218', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(7, 'S-*4Y1532084952', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(8, 'S-*4Y1532084952', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(9, 'S-A.-1532085589', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(10, 'S-A.-1532085589', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(11, 'S-,A51532085884', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(12, 'S-,A51532085884', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(13, 'S-DW.1532086714', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(14, 'S-DW.1532086714', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(15, 'S-GG01532088140', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(16, 'S-GG01532088140', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(17, 'S-QJ=1532088220', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(18, 'S-QJ=1532088220', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(19, 'S-SEL1532088272', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(20, 'S-SEL1532088272', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(21, 'S-NR61532088592', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(22, 'S-NR61532088592', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(23, 'S-MHY1532177220', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(24, 'S-MHY1532177220', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(25, 'S-QV01532177907', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(26, 'S-QV01532177907', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(27, 'S-RQA1532178466', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(28, 'S-RQA1532178466', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(29, 'S--0I1532178699', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(30, 'S--0I1532178699', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(31, 'S-3_L1532178820', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(32, 'S-3_L1532178820', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(33, 'S-L4B1532178873', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(34, 'S-L4B1532178873', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(35, 'S-,2C1532178951', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(36, 'S-,2C1532178951', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(37, 'S-K2X1532179390', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(38, 'S-K2X1532179390', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(39, 'S-AGC1532179443', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(40, 'S-AGC1532179443', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(41, 'S-4Q71532180588', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(42, 'S-4Q71532180588', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(43, 'S-S-+1532182513', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(44, 'S-S-+1532182513', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(45, 'S-1*V1532182846', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(46, 'S-1*V1532182846', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(47, 'S-2PS1532183181', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(48, 'S-2PS1532183181', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(49, 'S-15_1532183467', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(50, 'S-15_1532183467', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(51, 'S-G6X1532185454', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(52, 'S-G6X1532185454', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(53, 'S-.771532231693', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(54, 'S-F=Y1532232463', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(55, 'S-O,Z1532232924', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(56, 'S-O,Z1532232924', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(57, 'S-T=P1532233233', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(58, 'S-T=P1532233233', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(59, 'S-BUB1532233840', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(60, 'S-BUB1532233840', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(61, 'S-0ZN1532233969', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(62, 'S-0ZN1532233969', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(63, 'S-KAO1532234404', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(64, 'S-KAO1532234404', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(65, 'S-CZT1532234544', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(66, 'S-CZT1532234544', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(67, 'S-ADX1532235335', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(68, 'S-ADX1532235335', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(69, 'S-R3K1532235366', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(70, 'S-R3K1532235366', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, ''),
(71, 'S-NQ11532236082', 4, '', 'Beras putih', 0, '', 0, '', 301000, 1, ''),
(72, 'S-NQ11532236082', 6, '', 'Beras Hitam', 0, '', 0, '', 100100, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `log_anggota`
--

CREATE TABLE `log_anggota` (
  `id_log_anggota` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `longti` decimal(16,10) NOT NULL,
  `latitut` decimal(16,10) NOT NULL,
  `activedate` varchar(20) NOT NULL,
  `kegiatan` varchar(200) NOT NULL,
  `u_agent` varchar(200) NOT NULL,
  `ip_addr` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `londri`
--

CREATE TABLE `londri` (
  `id_londri` int(11) NOT NULL,
  `plu` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `tag` varchar(200) NOT NULL,
  `harga_beli` double NOT NULL,
  `harga_jual` double NOT NULL,
  `biaya_shipping` double NOT NULL,
  `harga_kilo_beli` double NOT NULL,
  `harga_kilo_jual` double NOT NULL,
  `tempo` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `diskon` double NOT NULL,
  `diskon_type` varchar(10) NOT NULL,
  `tax` double NOT NULL,
  `tax_type` varchar(10) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `londri`
--

INSERT INTO `londri` (`id_londri`, `plu`, `nama`, `keterangan`, `tag`, `harga_beli`, `harga_jual`, `biaya_shipping`, `harga_kilo_beli`, `harga_kilo_jual`, `tempo`, `id_kategori`, `id_mitra`, `status`, `diskon`, `diskon_type`, `tax`, `tax_type`, `aktif`, `userinsert`, `insertdate`, `useredit`, `editdate`) VALUES
(1, 'MACLA0001', 'Paket 3 Kilo', 'Paket 3 Kilo Keterangan', 'Paket 3 Kilo Keterangan', 1000, 2000, 1, 500, 1000, 3, 2, 1, '1', 0, '3', 0, '1', '1', 1, '1527502861', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mitra`
--

CREATE TABLE `mitra` (
  `id_mitra` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `area1` varchar(5) NOT NULL,
  `area2` varchar(5) NOT NULL,
  `area3` varchar(5) NOT NULL,
  `Jambuka` int(11) NOT NULL,
  `jamtutup` int(11) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mitra`
--

INSERT INTO `mitra` (`id_mitra`, `nama`, `keterangan`, `area1`, `area2`, `area3`, `Jambuka`, `jamtutup`, `alamat`, `photo`, `aktif`, `userinsert`, `insertdate`, `useredit`, `editdate`) VALUES
(1, 'ALEX\'s STORE', 'Jual Sembako dll', '0', '0', '0', 0, 0, 'Jakarta', '', '1', 1, '1527498764', 1, 1532334323);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id_order` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `1` int(11) NOT NULL,
  `2` int(11) NOT NULL,
  `3` int(11) NOT NULL,
  `4` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `plu` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `tag` varchar(200) NOT NULL,
  `harga_beli` double NOT NULL,
  `harga_jual` double NOT NULL,
  `biaya_shipping` double NOT NULL,
  `photo` varchar(30) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `id_speksi` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '3',
  `stok` double NOT NULL,
  `unit` varchar(10) NOT NULL,
  `diskon` double NOT NULL,
  `diskon_type` varchar(10) NOT NULL,
  `tax` double NOT NULL,
  `tax_type` varchar(10) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `plu`, `nama`, `keterangan`, `deskripsi`, `tag`, `harga_beli`, `harga_jual`, `biaya_shipping`, `photo`, `id_kategori`, `id_brand`, `id_speksi`, `id_mitra`, `status`, `stok`, `unit`, `diskon`, `diskon_type`, `tax`, `tax_type`, `aktif`, `userinsert`, `insertdate`, `useredit`, `editdate`) VALUES
(1, '222222', 'Kacang Mede', 'Kacang mede 1 Kg', '', 'kue kacang mede ', 50000, 55000, 5000, 'produk_1.jpg', 6, 2, 0, 0, '1', 10, '1', 0, '3', 0, '1', '1', 1, '1530862374', 1, '1527324766'),
(2, '1111111', 'Kangkung paket1', 'Kangkung', '', 'sayur sayuran kangkung dapur ', 1000, 1300, 200, 'produk_2.jpg', 4, 1, 0, 0, '3', 10, '2', 0, '3', 500, '1', '1', 1, '1527325784', 1, '1527324825'),
(3, '', '', '', '', '', 0, 0, 0, 'produk_3.', 0, 0, 0, 0, '1', 0, '', 0, '', 0, '', '1', 7, '1530171682', 0, ''),
(4, 'BRS01010101', 'Beras putih', 'beras putih menthik organik campur 25 kg pulen kualitas premium tanpa pemutih', '', 'beras putih menthik organik campur 25 kg pulen kualitas premium tanpa pemutih', 290000, 301000, 0, 'produk_4.jpg', 14, 2, 0, 1, '1', 10, '1', 0, '1', 0, '1', '1', 1, '1530850378', 1, '1532861584'),
(5, 'BRS01010102', 'Beras Merah', 'beras merah organik 5 kg asli tanpa campuran', '', 'beras merah organik 5 kg asli tanpa campuran', 70000, 76500, 0, 'produk_5.jpg', 14, 2, 0, 1, '1', 10, '1', 0, '1', 0, '1', '1', 1, '1530859847', 1, '1532861598'),
(6, 'BRS01010103', 'Beras Hitam', 'beras hitam organik 5kg asli tanpa campuran', '', 'beras hitam organik 5kg asli tanpa campuran', 99000, 100100, 0, 'produk_6.jpg', 14, 2, 0, 1, '1', 10, '1', 0, '1', 0, '1', '1', 1, '1530862002', 1, '1532861614'),
(7, 'BRS01010104', 'Beras basmati', 'Beras basmati Sella Parboiled eceran 500 gr', '', 'Beras basmati Sella Parboiled eceran 500 gr', 38000, 39000, 0, 'produk_7.png', 14, 2, 0, 1, '1', 10, '1', 0, '1', 0, '1', '1', 1, '1530862329', 1, '1532861640'),
(8, 'GL000000010111', 'Gula pasir putih Gulaku 1 kg', 'Gula pasir putih Gulaku satu dus isi 24 kg @ 1 kg', '', 'Gula pasir putih Gulaku satu dus isi 24 kg @ 1 kg', 299000, 300000, 0, 'produk_8.png', 15, 1, 0, 1, '1', 10, '1', 0, '1', 0, '1', '1', 1, '1532876422', 0, ''),
(9, 'GL000000010144', 'Gula pasir gula kristal putih ', 'Gula pasir gula kristal putih lokal', '', 'Gula pasir gula kristal putih lokal', 11000, 12000, 0, 'produk_9.jpg', 15, 1, 0, 1, '1', 0, '1', 0, '1', 0, '1', '1', 1, '1532877655', 0, ''),
(10, 'TL000011111', 'Telur Ayam Kampung Organic', 'Telur Ayam Kampung Organic', '', 'Telur Ayam Kampung Organic', 35000, 36000, 0, 'produk_10.jpeg', 13, 2, 0, 1, '1', 0, '1', 0, '1', 0, '1', '1', 1, '1532880374', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id_slider` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `ket_detail` text NOT NULL,
  `photo_kecil` varchar(30) NOT NULL,
  `photo_besar` varchar(30) NOT NULL,
  `urut` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id_slider`, `nama`, `keterangan`, `ket_detail`, `photo_kecil`, `photo_besar`, `urut`, `status`, `aktif`, `userinsert`, `insertdate`, `useredit`, `editdate`) VALUES
(6, 'Delevery Voucer Rp.20 000', 'Delevery Voucer Rp.20 000', '', 'photokecil_6.jpg', '', 1, '1', '1', 1, '1533715371', 0, ''),
(7, 'Free Deliveri Order', 'Free Deliveri Order', '', 'photokecil_7.jpg', '', 2, '1', '1', 1, '1533715440', 0, ''),
(8, 'Beli 1 Gratis SATU', 'Beli 1 Gratis SATU', '', 'photokecil_8.jpg', '', 3, '1', '1', 1, '1533715491', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `sub_kategori`
--

CREATE TABLE `sub_kategori` (
  `id_sub_katagori` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `kode_katagori` varchar(5) NOT NULL,
  `halaman` varchar(5) NOT NULL,
  `data_brand` varchar(200) NOT NULL,
  `data_vendor` varchar(200) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id_unit` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id_unit`, `nama`, `jumlah`, `satuan`) VALUES
(1, 'pc', 1, '1'),
(2, 'RIM', 500, 'LEMBAR');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `userlogin` varchar(50) NOT NULL,
  `userpass` varchar(45) NOT NULL,
  `token` varchar(45) NOT NULL,
  `akses` varchar(3) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '1',
  `userinsert` int(11) NOT NULL,
  `insertdate` varchar(20) NOT NULL,
  `useredit` int(11) NOT NULL,
  `editdate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `keterangan`, `userlogin`, `userpass`, `token`, `akses`, `aktif`, `userinsert`, `insertdate`, `useredit`, `editdate`) VALUES
(1, 'Machfudh', 'Test ke 2', 'machfudh@gmail.com', '6eb47f188a8c96a37321b699a26cc6fe', '', '257', '1', 1, '1527218865', 0, '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_alamat`
-- (See below for the actual view)
--
CREATE TABLE `v_alamat` (
`id_alamat` int(11)
,`id_anggota` int(11)
,`nama` varchar(30)
,`jalan` varchar(50)
,`no_rumah` varchar(4)
,`kota` int(11)
,`kecamatan` int(11)
,`kelurahan` int(11)
,`kode` varchar(1)
,`longti` decimal(16,10)
,`latitut` decimal(16,10)
,`namakel` varchar(30)
,`kodepos` varchar(5)
,`namakec` varchar(30)
,`namakota` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_brand`
-- (See below for the actual view)
--
CREATE TABLE `v_brand` (
`id_brand` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`photo` varchar(30)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_homeslider`
-- (See below for the actual view)
--
CREATE TABLE `v_homeslider` (
`id_slider` int(11)
,`nama` varchar(30)
,`keterangan` varchar(200)
,`ket_detail` text
,`photo_kecil` varchar(30)
,`photo_besar` varchar(30)
,`urut` int(11)
,`status` varchar(1)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_image_produk`
-- (See below for the actual view)
--
CREATE TABLE `v_image_produk` (
`id_image_produk` int(11)
,`id_produk` int(11)
,`nama` varchar(30)
,`photo` varchar(30)
,`urut` int(11)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoiceorder`
-- (See below for the actual view)
--
CREATE TABLE `v_invoiceorder` (
`id_invoiceorder` int(11)
,`id_anggota` int(11)
,`nama` varchar(30)
,`phone` varchar(20)
,`catatan` text
,`invoice_no` varchar(20)
,`invoice_date` varchar(20)
,`id_pengiriman` int(11)
,`id_shipping` int(11)
,`shipping_date` varchar(20)
,`shipping_status` int(11)
,`shipping_ket` varchar(200)
,`shipping_add` varchar(200)
,`id_bank` int(11)
,`vat` int(11)
,`vat_persen` int(11)
,`payment_type` int(11)
,`payment_status` int(11)
,`payment_detail` text
,`payment_date` varchar(20)
,`grand_total` double
,`status` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_katagori`
-- (See below for the actual view)
--
CREATE TABLE `v_katagori` (
`id_katagori` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`kode_katagori` varchar(5)
,`halaman` varchar(5)
,`urut` int(11)
,`lavel` int(11)
,`parent` int(11)
,`data_brand` varchar(200)
,`data_vendor` varchar(200)
,`photo` varchar(30)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_katagori1`
-- (See below for the actual view)
--
CREATE TABLE `v_katagori1` (
`id_katagori` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`kode_katagori` varchar(5)
,`halaman` varchar(5)
,`urut` int(11)
,`lavel` int(11)
,`parent` int(11)
,`data_brand` varchar(200)
,`data_vendor` varchar(200)
,`photo` varchar(30)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_katagori2`
-- (See below for the actual view)
--
CREATE TABLE `v_katagori2` (
`id_katagori` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`kode_katagori` varchar(5)
,`halaman` varchar(5)
,`urut` int(11)
,`lavel` int(11)
,`parent` int(11)
,`data_brand` varchar(200)
,`data_vendor` varchar(200)
,`photo` varchar(30)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_katagori3`
-- (See below for the actual view)
--
CREATE TABLE `v_katagori3` (
`id_katagori` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`kode_katagori` varchar(5)
,`halaman` varchar(5)
,`urut` int(11)
,`lavel` int(11)
,`parent` int(11)
,`data_brand` varchar(200)
,`data_vendor` varchar(200)
,`photo` varchar(30)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_katagori4`
-- (See below for the actual view)
--
CREATE TABLE `v_katagori4` (
`id_katagori` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`kode_katagori` varchar(5)
,`halaman` varchar(5)
,`urut` int(11)
,`lavel` int(11)
,`parent` int(11)
,`data_brand` varchar(200)
,`data_vendor` varchar(200)
,`photo` varchar(30)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_katagori5`
-- (See below for the actual view)
--
CREATE TABLE `v_katagori5` (
`id_katagori` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`kode_katagori` varchar(5)
,`halaman` varchar(5)
,`urut` int(11)
,`lavel` int(11)
,`parent` int(11)
,`data_brand` varchar(200)
,`data_vendor` varchar(200)
,`photo` varchar(30)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_londri`
-- (See below for the actual view)
--
CREATE TABLE `v_londri` (
`id_londri` int(11)
,`plu` varchar(30)
,`nama` varchar(30)
,`keterangan` varchar(200)
,`tag` varchar(200)
,`harga_beli` double
,`harga_jual` double
,`biaya_shipping` double
,`harga_kilo_beli` double
,`harga_kilo_jual` double
,`tempo` int(11)
,`id_kategori` int(11)
,`id_mitra` int(11)
,`status` varchar(1)
,`diskon` double
,`diskon_type` varchar(10)
,`tax` double
,`tax_type` varchar(10)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` int(11)
,`namakatagori` varchar(20)
,`namamitra` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_mitra`
-- (See below for the actual view)
--
CREATE TABLE `v_mitra` (
`id_mitra` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`area1` varchar(5)
,`area2` varchar(5)
,`area3` varchar(5)
,`alamat` varchar(200)
,`photo` varchar(30)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_produk`
-- (See below for the actual view)
--
CREATE TABLE `v_produk` (
`id_produk` int(11)
,`plu` varchar(30)
,`nama` varchar(30)
,`keterangan` varchar(200)
,`deskripsi` text
,`tag` varchar(200)
,`harga_beli` double
,`harga_jual` double
,`biaya_shipping` double
,`photo` varchar(30)
,`id_kategori` int(11)
,`id_brand` int(11)
,`id_speksi` int(11)
,`id_mitra` int(11)
,`status` varchar(1)
,`stok` double
,`unit` varchar(10)
,`diskon` double
,`diskon_type` varchar(10)
,`tax` double
,`tax_type` varchar(10)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
,`kode_katagori` varchar(5)
,`namakatagori` varchar(20)
,`namabrand` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_slider`
-- (See below for the actual view)
--
CREATE TABLE `v_slider` (
`id_slider` int(11)
,`nama` varchar(30)
,`keterangan` varchar(200)
,`ket_detail` text
,`photo_kecil` varchar(30)
,`photo_besar` varchar(30)
,`urut` int(11)
,`status` varchar(1)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_unit`
-- (See below for the actual view)
--
CREATE TABLE `v_unit` (
`id_unit` int(11)
,`nama` varchar(20)
,`jumlah` int(11)
,`satuan` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_user`
-- (See below for the actual view)
--
CREATE TABLE `v_user` (
`id_user` int(11)
,`nama` varchar(20)
,`keterangan` varchar(200)
,`userlogin` varchar(50)
,`userpass` varchar(45)
,`token` varchar(45)
,`akses` varchar(3)
,`aktif` varchar(1)
,`userinsert` int(11)
,`insertdate` varchar(20)
,`useredit` int(11)
,`editdate` varchar(20)
);

-- --------------------------------------------------------

--
-- Structure for view `v_alamat`
--
DROP TABLE IF EXISTS `v_alamat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_alamat`  AS  select `a`.`id_alamat` AS `id_alamat`,`a`.`id_anggota` AS `id_anggota`,`a`.`nama` AS `nama`,`a`.`jalan` AS `jalan`,`a`.`no_rumah` AS `no_rumah`,`a`.`kota` AS `kota`,`a`.`kecamatan` AS `kecamatan`,`a`.`kelurahan` AS `kelurahan`,`a`.`kode` AS `kode`,`a`.`longti` AS `longti`,`a`.`latitut` AS `latitut`,`k`.`namakel` AS `namakel`,`k`.`kodepos` AS `kodepos`,`c`.`namakec` AS `namakec`,`t`.`namakota` AS `namakota` from (((`alamat` `a` join `kelurahan` `k` on(`k`.`id` = `a`.`kelurahan`)) join `kecamatan` `c` on(`c`.`id` = `a`.`kecamatan`)) join `kota` `t` on(`t`.`id` = `a`.`kota`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_brand`
--
DROP TABLE IF EXISTS `v_brand`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_brand`  AS  select `brand`.`id_brand` AS `id_brand`,`brand`.`nama` AS `nama`,`brand`.`keterangan` AS `keterangan`,`brand`.`photo` AS `photo`,`brand`.`aktif` AS `aktif`,`brand`.`userinsert` AS `userinsert`,`brand`.`insertdate` AS `insertdate`,`brand`.`useredit` AS `useredit`,`brand`.`editdate` AS `editdate` from `brand` where `brand`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_homeslider`
--
DROP TABLE IF EXISTS `v_homeslider`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_homeslider`  AS  select `s`.`id_slider` AS `id_slider`,`s`.`nama` AS `nama`,`s`.`keterangan` AS `keterangan`,`s`.`ket_detail` AS `ket_detail`,`s`.`photo_kecil` AS `photo_kecil`,`s`.`photo_besar` AS `photo_besar`,`s`.`urut` AS `urut`,`s`.`status` AS `status`,`s`.`aktif` AS `aktif`,`s`.`userinsert` AS `userinsert`,`s`.`insertdate` AS `insertdate`,`s`.`useredit` AS `useredit`,`s`.`editdate` AS `editdate` from `v_slider` `s` where `s`.`status` = 1 order by `s`.`urut` ;

-- --------------------------------------------------------

--
-- Structure for view `v_image_produk`
--
DROP TABLE IF EXISTS `v_image_produk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_image_produk`  AS  select `i`.`id_image_produk` AS `id_image_produk`,`i`.`id_produk` AS `id_produk`,`i`.`nama` AS `nama`,`i`.`photo` AS `photo`,`i`.`urut` AS `urut`,`i`.`aktif` AS `aktif`,`i`.`userinsert` AS `userinsert`,`i`.`insertdate` AS `insertdate`,`i`.`useredit` AS `useredit`,`i`.`editdate` AS `editdate` from `image_produk` `i` where `i`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoiceorder`
--
DROP TABLE IF EXISTS `v_invoiceorder`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_invoiceorder`  AS  select `i`.`id_invoiceorder` AS `id_invoiceorder`,`i`.`id_anggota` AS `id_anggota`,`i`.`nama` AS `nama`,`i`.`phone` AS `phone`,`i`.`catatan` AS `catatan`,`i`.`invoice_no` AS `invoice_no`,`i`.`invoice_date` AS `invoice_date`,`i`.`id_pengiriman` AS `id_pengiriman`,`i`.`id_shipping` AS `id_shipping`,`i`.`shipping_date` AS `shipping_date`,`i`.`shipping_status` AS `shipping_status`,`i`.`shipping_ket` AS `shipping_ket`,`i`.`shipping_add` AS `shipping_add`,`i`.`id_bank` AS `id_bank`,`i`.`vat` AS `vat`,`i`.`vat_persen` AS `vat_persen`,`i`.`payment_type` AS `payment_type`,`i`.`payment_status` AS `payment_status`,`i`.`payment_detail` AS `payment_detail`,`i`.`payment_date` AS `payment_date`,`i`.`grand_total` AS `grand_total`,`i`.`status` AS `status` from `invoiceorder` `i` where `i`.`status` <> 0 ;

-- --------------------------------------------------------

--
-- Structure for view `v_katagori`
--
DROP TABLE IF EXISTS `v_katagori`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_katagori`  AS  select `k`.`id_katagori` AS `id_katagori`,`k`.`nama` AS `nama`,`k`.`keterangan` AS `keterangan`,`k`.`kode_katagori` AS `kode_katagori`,`k`.`halaman` AS `halaman`,`k`.`urut` AS `urut`,`k`.`lavel` AS `lavel`,`k`.`parent` AS `parent`,`k`.`data_brand` AS `data_brand`,`k`.`data_vendor` AS `data_vendor`,`k`.`photo` AS `photo`,`k`.`aktif` AS `aktif`,`k`.`userinsert` AS `userinsert`,`k`.`insertdate` AS `insertdate`,`k`.`useredit` AS `useredit`,`k`.`editdate` AS `editdate` from `katagori` `k` where `k`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_katagori1`
--
DROP TABLE IF EXISTS `v_katagori1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_katagori1`  AS  select `k`.`id_katagori` AS `id_katagori`,`k`.`nama` AS `nama`,`k`.`keterangan` AS `keterangan`,`k`.`kode_katagori` AS `kode_katagori`,`k`.`halaman` AS `halaman`,`k`.`urut` AS `urut`,`k`.`lavel` AS `lavel`,`k`.`parent` AS `parent`,`k`.`data_brand` AS `data_brand`,`k`.`data_vendor` AS `data_vendor`,`k`.`photo` AS `photo`,`k`.`aktif` AS `aktif`,`k`.`userinsert` AS `userinsert`,`k`.`insertdate` AS `insertdate`,`k`.`useredit` AS `useredit`,`k`.`editdate` AS `editdate` from `katagori` `k` where `k`.`lavel` = 1 and `k`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_katagori2`
--
DROP TABLE IF EXISTS `v_katagori2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_katagori2`  AS  select `k`.`id_katagori` AS `id_katagori`,`k`.`nama` AS `nama`,`k`.`keterangan` AS `keterangan`,`k`.`kode_katagori` AS `kode_katagori`,`k`.`halaman` AS `halaman`,`k`.`urut` AS `urut`,`k`.`lavel` AS `lavel`,`k`.`parent` AS `parent`,`k`.`data_brand` AS `data_brand`,`k`.`data_vendor` AS `data_vendor`,`k`.`photo` AS `photo`,`k`.`aktif` AS `aktif`,`k`.`userinsert` AS `userinsert`,`k`.`insertdate` AS `insertdate`,`k`.`useredit` AS `useredit`,`k`.`editdate` AS `editdate` from `katagori` `k` where `k`.`lavel` = 2 and `k`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_katagori3`
--
DROP TABLE IF EXISTS `v_katagori3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_katagori3`  AS  select `k`.`id_katagori` AS `id_katagori`,`k`.`nama` AS `nama`,`k`.`keterangan` AS `keterangan`,`k`.`kode_katagori` AS `kode_katagori`,`k`.`halaman` AS `halaman`,`k`.`urut` AS `urut`,`k`.`lavel` AS `lavel`,`k`.`parent` AS `parent`,`k`.`data_brand` AS `data_brand`,`k`.`data_vendor` AS `data_vendor`,`k`.`photo` AS `photo`,`k`.`aktif` AS `aktif`,`k`.`userinsert` AS `userinsert`,`k`.`insertdate` AS `insertdate`,`k`.`useredit` AS `useredit`,`k`.`editdate` AS `editdate` from `katagori` `k` where `k`.`lavel` = 3 and `k`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_katagori4`
--
DROP TABLE IF EXISTS `v_katagori4`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_katagori4`  AS  select `k`.`id_katagori` AS `id_katagori`,`k`.`nama` AS `nama`,`k`.`keterangan` AS `keterangan`,`k`.`kode_katagori` AS `kode_katagori`,`k`.`halaman` AS `halaman`,`k`.`urut` AS `urut`,`k`.`lavel` AS `lavel`,`k`.`parent` AS `parent`,`k`.`data_brand` AS `data_brand`,`k`.`data_vendor` AS `data_vendor`,`k`.`photo` AS `photo`,`k`.`aktif` AS `aktif`,`k`.`userinsert` AS `userinsert`,`k`.`insertdate` AS `insertdate`,`k`.`useredit` AS `useredit`,`k`.`editdate` AS `editdate` from `katagori` `k` where `k`.`lavel` = 4 and `k`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_katagori5`
--
DROP TABLE IF EXISTS `v_katagori5`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_katagori5`  AS  select `k`.`id_katagori` AS `id_katagori`,`k`.`nama` AS `nama`,`k`.`keterangan` AS `keterangan`,`k`.`kode_katagori` AS `kode_katagori`,`k`.`halaman` AS `halaman`,`k`.`urut` AS `urut`,`k`.`lavel` AS `lavel`,`k`.`parent` AS `parent`,`k`.`data_brand` AS `data_brand`,`k`.`data_vendor` AS `data_vendor`,`k`.`photo` AS `photo`,`k`.`aktif` AS `aktif`,`k`.`userinsert` AS `userinsert`,`k`.`insertdate` AS `insertdate`,`k`.`useredit` AS `useredit`,`k`.`editdate` AS `editdate` from `katagori` `k` where `k`.`lavel` = 5 and `k`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_londri`
--
DROP TABLE IF EXISTS `v_londri`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_londri`  AS  select `l`.`id_londri` AS `id_londri`,`l`.`plu` AS `plu`,`l`.`nama` AS `nama`,`l`.`keterangan` AS `keterangan`,`l`.`tag` AS `tag`,`l`.`harga_beli` AS `harga_beli`,`l`.`harga_jual` AS `harga_jual`,`l`.`biaya_shipping` AS `biaya_shipping`,`l`.`harga_kilo_beli` AS `harga_kilo_beli`,`l`.`harga_kilo_jual` AS `harga_kilo_jual`,`l`.`tempo` AS `tempo`,`l`.`id_kategori` AS `id_kategori`,`l`.`id_mitra` AS `id_mitra`,`l`.`status` AS `status`,`l`.`diskon` AS `diskon`,`l`.`diskon_type` AS `diskon_type`,`l`.`tax` AS `tax`,`l`.`tax_type` AS `tax_type`,`l`.`aktif` AS `aktif`,`l`.`userinsert` AS `userinsert`,`l`.`insertdate` AS `insertdate`,`l`.`useredit` AS `useredit`,`l`.`editdate` AS `editdate`,`k`.`nama` AS `namakatagori`,`m`.`nama` AS `namamitra` from ((`londri` `l` join `katagori` `k` on(`k`.`id_katagori` = `l`.`id_kategori`)) join `mitra` `m` on(`m`.`id_mitra` = `l`.`id_mitra`)) where `l`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_mitra`
--
DROP TABLE IF EXISTS `v_mitra`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_mitra`  AS  select `m`.`id_mitra` AS `id_mitra`,`m`.`nama` AS `nama`,`m`.`keterangan` AS `keterangan`,`m`.`area1` AS `area1`,`m`.`area2` AS `area2`,`m`.`area3` AS `area3`,`m`.`alamat` AS `alamat`,`m`.`photo` AS `photo`,`m`.`aktif` AS `aktif`,`m`.`userinsert` AS `userinsert`,`m`.`insertdate` AS `insertdate`,`m`.`useredit` AS `useredit`,`m`.`editdate` AS `editdate` from `mitra` `m` where `m`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_produk`
--
DROP TABLE IF EXISTS `v_produk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_produk`  AS  select `p`.`id_produk` AS `id_produk`,`p`.`plu` AS `plu`,`p`.`nama` AS `nama`,`p`.`keterangan` AS `keterangan`,`p`.`deskripsi` AS `deskripsi`,`p`.`tag` AS `tag`,`p`.`harga_beli` AS `harga_beli`,`p`.`harga_jual` AS `harga_jual`,`p`.`biaya_shipping` AS `biaya_shipping`,`p`.`photo` AS `photo`,`p`.`id_kategori` AS `id_kategori`,`p`.`id_brand` AS `id_brand`,`p`.`id_speksi` AS `id_speksi`,`p`.`id_mitra` AS `id_mitra`,`p`.`status` AS `status`,`p`.`stok` AS `stok`,`p`.`unit` AS `unit`,`p`.`diskon` AS `diskon`,`p`.`diskon_type` AS `diskon_type`,`p`.`tax` AS `tax`,`p`.`tax_type` AS `tax_type`,`p`.`aktif` AS `aktif`,`p`.`userinsert` AS `userinsert`,`p`.`insertdate` AS `insertdate`,`p`.`useredit` AS `useredit`,`p`.`editdate` AS `editdate`,`k`.`kode_katagori` AS `kode_katagori`,`k`.`nama` AS `namakatagori`,`b`.`nama` AS `namabrand` from ((`produk` `p` join `katagori` `k` on(`k`.`id_katagori` = `p`.`id_kategori`)) join `brand` `b` on(`b`.`id_brand` = `p`.`id_brand`)) where `p`.`aktif` = 1 and `p`.`status` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_slider`
--
DROP TABLE IF EXISTS `v_slider`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_slider`  AS  select `s`.`id_slider` AS `id_slider`,`s`.`nama` AS `nama`,`s`.`keterangan` AS `keterangan`,`s`.`ket_detail` AS `ket_detail`,`s`.`photo_kecil` AS `photo_kecil`,`s`.`photo_besar` AS `photo_besar`,`s`.`urut` AS `urut`,`s`.`status` AS `status`,`s`.`aktif` AS `aktif`,`s`.`userinsert` AS `userinsert`,`s`.`insertdate` AS `insertdate`,`s`.`useredit` AS `useredit`,`s`.`editdate` AS `editdate` from `slider` `s` where `s`.`aktif` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_unit`
--
DROP TABLE IF EXISTS `v_unit`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_unit`  AS  select `unit`.`id_unit` AS `id_unit`,`unit`.`nama` AS `nama`,`unit`.`jumlah` AS `jumlah`,`unit`.`satuan` AS `satuan` from `unit` ;

-- --------------------------------------------------------

--
-- Structure for view `v_user`
--
DROP TABLE IF EXISTS `v_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`machfud1`@`localhost` SQL SECURITY DEFINER VIEW `v_user`  AS  select `user`.`id_user` AS `id_user`,`user`.`nama` AS `nama`,`user`.`keterangan` AS `keterangan`,`user`.`userlogin` AS `userlogin`,`user`.`userpass` AS `userpass`,`user`.`token` AS `token`,`user`.`akses` AS `akses`,`user`.`aktif` AS `aktif`,`user`.`userinsert` AS `userinsert`,`user`.`insertdate` AS `insertdate`,`user`.`useredit` AS `useredit`,`user`.`editdate` AS `editdate` from `user` where `user`.`aktif` = 1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alamat`
--
ALTER TABLE `alamat`
  ADD PRIMARY KEY (`id_alamat`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `anggotalog`
--
ALTER TABLE `anggotalog`
  ADD PRIMARY KEY (`id_anggotalog`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id_brand`);

--
-- Indexes for table `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id_halaman`);

--
-- Indexes for table `image_produk`
--
ALTER TABLE `image_produk`
  ADD PRIMARY KEY (`id_image_produk`);

--
-- Indexes for table `invoiceorder`
--
ALTER TABLE `invoiceorder`
  ADD PRIMARY KEY (`id_invoiceorder`);

--
-- Indexes for table `katagori`
--
ALTER TABLE `katagori`
  ADD PRIMARY KEY (`id_katagori`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listorder`
--
ALTER TABLE `listorder`
  ADD PRIMARY KEY (`id_listorder`);

--
-- Indexes for table `log_anggota`
--
ALTER TABLE `log_anggota`
  ADD PRIMARY KEY (`id_log_anggota`);

--
-- Indexes for table `londri`
--
ALTER TABLE `londri`
  ADD PRIMARY KEY (`id_londri`);

--
-- Indexes for table `mitra`
--
ALTER TABLE `mitra`
  ADD PRIMARY KEY (`id_mitra`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `sub_kategori`
--
ALTER TABLE `sub_kategori`
  ADD PRIMARY KEY (`id_sub_katagori`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id_unit`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alamat`
--
ALTER TABLE `alamat`
  MODIFY `id_alamat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `katagori`
--
ALTER TABLE `katagori`
  MODIFY `id_katagori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
